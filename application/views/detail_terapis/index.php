<div style="padding-top: 100px"></div>

<div class="col-12 container row res-p-4" id="main-content" style="margin-bottom: 20px">

    <div class="col-lg-4 col-md-5 p-0 sidebar">
        <!-- ----- TERAPIS ATRIBUTE ----- -->
        <div class="col-12 card-transparent p-4 d-flex flex-column align-items-center">
            <img id="profile" class="img-profile mt-4" style="width: 120px; height: 120px" src="<?php echo base_url('assets/img/img1.jpg') ?>">
            <h3 class="mt-4" id="username"></h3>
            <!-- <div class="d-flex align-items-center mt-2">
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>

                <h4 class="ml-2 color-primary">4.7</h4>
            </div> -->

            <!-- <a class="btn btn-primary w-100 paragraph-medium color-white mt-4">Hubungi Terapis</a> -->

            <hr class="w-100" style="border: #393939 solid 1px; opacity: .08; margin: 32px auto">

            <div class="w-100 d-flex justify-content-between">
                <p class="paragraph-medium">Asal</p>
                <p class="paragraph-medium" id="alamat"></p>
            </div>

            <div class="w-100 d-flex justify-content-between mt-2 mb-2">
                <p class="paragraph-medium">Bergabung</p>
                <p class="paragraph-medium" id="join"></p>
            </div>
        </div>

        <!-- ----- DESKRIPSI ----- -->
        <div class="col-12 card-transparent p-4 mt-4">
            <h3>Deskripsi</h3>
            <p class="paragraph-small mt-2" id="desc"></p>
        </div>
    </div>

    <div class="col-lg-8 col-md-7 p-0">
        <!-- ----- LAYANAN TERAPIS ----- -->
        <div class="col-12 container row grid-2-alt" id="mitraGigsList">
        </div>

        <!-- ----- TANGGAPAN DAN ULASAN ----- -->
        <div id="ratingContainer" class="mt-4" style="display: none">
            <h2>Tanggapan & Ulasan</h2>
            <div class="col-12 p-0 mt-3 mb-4" id="ratingList">
            </div>
        </div>
    </div>
</div>

<script>
    var request;
    var requestGigs;
    var requestRating;
    var nestedrequest;

    var mitraData;
    var mitraGigs;
    var gigsRating;

    var id;
    var value;


    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request || requestGigs) {
            request.abort();
            requestGigs.abort();
        }

        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        id = getUrlParameter('id');

        request = $.ajax({
            url: 'http://103.129.223.136:2099/user/mitra/' + id,
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            mitraData = response.data;

            console.log(response);

            if (success) {
                $("#username").append(mitraData[0].name);
                $("#alamat").append(mitraData[0].kota + ', ' + mitraData[0].provinsi);
                $("#join").append(moment(mitraData[0].date_created).format("MMMM YYYY"));
                $("#desc").append(mitraData[0].description);
                $("#profile").attr("src", mitraData[0].photo);
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestGigs = $.ajax({
            url: 'http://103.129.223.136:2099/gigs/mitra/' + id,
            type: 'get',
            data: {

            }
        });

        requestGigs.done(function(response) {
            var success = response.success;
            var message = response.message;
            mitraData = response.data;

            console.log(response);

            if (success) {
                for (i = 0; i < mitraData.length; i++) {
                    value = mitraData[i];

                    nestedrequest = $.ajax({
                        url: 'http://103.129.223.136:2099/gigs-package/' + value.id,
                        type: 'get',
                        data: {

                        }
                    });

                    nestedrequest.done(function(response) {
                        var success = response.success;
                        var message = response.message;
                        var data = response.data;

                        console.log(response);

                        if (success) {
                            if (data[0] && data[data.length - 1]) {
                                var lowprice = data[0].price;
                                var highprice = data[data.length - 1].price;
                                var priceGigs = "Rp " + numberWithCommas(lowprice) + " - Rp " + numberWithCommas(highprice);

                            }
                        } else {
                            console.error(response);
                        }
                    });

                    nestedrequest.fail(function(response) {
                        console.error(response);
                    });

                    requestRating = $.ajax({
                        url: 'http://103.129.223.136:2099/gigs-rate/' + value.id,
                        type: 'get',
                        data: {

                        }
                    });

                    requestRating.done(function(response) {
                        var success = response.success;
                        var message = response.message;
                        gigsRating = response.data;

                        console.log(response);

                        if (success) {
                            if (gigsRating.length > 0) {
                                document.getElementById('ratingContainer').style.display = 'block';
                            }
                            for (i = 0; i < gigsRating.length; i++) {
                                var value = gigsRating[i];
                                $("#ratingList").append(`
                                    <div class="d-flex mt-4">
                                        <a href="#" class="img-profile ml-2 d-flex justify-content-center align-items-center" style="background-color: rgba(57, 57, 57, 0.08)">
                                            <h2>` + value.name.substring(0, 1) + `</h2>
                                        </a>
                                        <div class="ml-3">
                                            <div class="d-flex">
                                                <p class="paragraph-medium" style="font-weight: 500">` + value.name + `</p>
                                                <i class='bx bxs-star color-primary ml-2' style="font-size: 20px;"></i>
                                                <h4 class="ml-1 color-primary">` + value.rating + `</h4>
                                            </div>
                                            <p class="paragraph-small mt-2">` + value.comment + `</p>
                                        </div>
                                    </div>
                                `);
                            }
                        } else {
                            console.error(response);
                        }
                    });

                    requestRating.fail(function(response) {
                        var success = response.success;
                        var message = response.message;
                        var data = response.data;

                        console.error(response);
                    });

                    if (i == 3) {
                        $("#mitraGigsList").append(`
                            <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + value.id + `'" style="cursor: pointer;" tabindex="1" class="card-item d-flex flex-column grid-3-child-hide">
                                <img src="<?php echo base_url('assets/package/master/'); ?>` + value.image + `" class="br-12 w-100" style="height: 220px; object-fit: cover;">
                                <div class="d-flex align-items-start justify-content-between card-item-title" style="flex: 1;">
                                    <h3 class="mt-2">` + value.name + `</h3>
                                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.mitra_id + `"><img src="` + value.mitra_photo + `" class="img-profile"></a>
                                </div>
                                <div class="d-flex align-items-center justify-content-between card-item-price">
                                    <div class="d-flex align-items-center">
                                        <i class='bx bxs-star color-primary mr-1' style="font-size: 24px;"></i>
                                        <p class="paragraph-small color-primary" id="ratingGigs-` + value.id + `"></p>
                                    </div>
                                    <p class="paragraph-small color-primary"><strong id="priceGigs-` + value.id + `"></strong></p>
                                </div>
                            </div>
                        `);
                    } else {
                        $("#mitraGigsList").append(`
                            <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + value.id + `'" style="cursor: pointer;" tabindex="1" class="card-item d-flex flex-column">
                                <img src="<?php echo base_url('assets/package/master/'); ?>` + value.image + `" class="br-12 w-100" style="height: 220px; object-fit: cover;">
                                <div class="d-flex align-items-start justify-content-between card-item-title" style="flex: 1;">
                                    <h3 class="mt-2">` + value.name + `</h3>
                                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.mitra_id + `"><img src="` + value.mitra_photo + `" class="img-profile"></a>
                                </div>
                                <div class="d-flex align-items-center justify-content-between card-item-price">
                                    <div class="d-flex align-items-center">
                                        <i class='bx bxs-star color-primary mr-1' style="font-size: 24px;"></i>
                                        <p class="paragraph-small color-primary" id="ratingGigs-` + value.id + `"></p>
                                    </div>
                                    <p class="paragraph-small color-primary"><strong id="priceGigs-` + value.id + `"></strong></p>
                                </div>
                            </div>
                        `);
                    }
                }

                request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs-rate-averages',
                    type: 'get',
                    data: {

                    }
                });

                request.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.log(response);

                    if (success) {
                        for (i = 0; i < data.length; i++) {
                            var rate = data[i].rate;
                            rate == null ? rate = '-' : rate = rate.toFixed(1);
                            $("#ratingGigs-" +  data[i].gigs_id).append(rate);
                        }
                    } else {
                        console.error(response);
                    }
                });

                request.fail(function(response) {
                    console.error(response);
                });
            } else {
                console.error(response);
            }
        });

        requestGigs.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });
</script>