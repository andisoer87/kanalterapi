<script>
  $(document).ready(function(){

    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

    show_product();
    
    function show_product(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/public/gigs',
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;
                  html += '<tr>'+
                            '<th>'+count+'.</th>'+
                            '<th>'+obj.data[i].name+'</th>'+
                            '<th class="text-center"><img src="<?php echo base_url(); ?>/assets/package/master/'+obj.data[i].image+'" height="100"></th>'+
                            '<th style="max-width:80px">'+
                              '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                                '<i class="fas fa-cog"></i>'+
                              '</button> '+
                              '<div class="dropdown-menu">'+
                                '<a class="dropdown-item" href="#">Detail</a>'+
                                '<a class="dropdown-item" data-toggle="modal" data-target="#edit-package">Edit</a>'+
                                '<a class="dropdown-item" data-toggle="modal" data-target="#delete-package">Delete</a>'+
                              '</div>'+
                              '<a href="<?php echo base_url('index.php/admin_package_control/sub_package?var1='); ?>'+obj.data[i].id+'&var2='+obj.data[i].name+'" class="btn bg-lightblue">'+
                                '<i class="fas fa-boxes"></i> daftar paket'+
                              '</a>'+
                            '</th>'+
                          '</tr>';
              }
              $('#show-data').html(html);
          }
      });
    }
    $('#btn_save').on('click',function(){
      
      var name  = $('#input-name').val();
      var image = $('#input-image').val();
      var desc  = $('#input-desc').val();

      request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs',
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        name: name,
                        image: 'terapi1.jpg',
                        desc: desc,
                    }
                });
      request.done(function(response) {
          $('#add-package').modal('hide');
          show_product();
          toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.');
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });
</script>