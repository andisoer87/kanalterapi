<script>
  $(document).ready(function(){

    show_product();
    
    function show_product(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/schedule/admin',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<td>'+
                                obj.data[i].gigs_name+' ('+obj.data[i].gigs_package_name+')'+
                            '</td>'+
                            '<td>'+
                                obj.data[i].date_proposed.substr(0,10)+
                            '</td>'+
                            '<td>'+
                                obj.data[i].date_proposed.substr(11,5)+
                            '</td>'+
                        '</tr>';
              }
              $('#schedule-display').html(html);
          }
      });
    }
    
    $('#btn_reschedule').on('click',function(){

      var id = $('#id').val();
      var date = $('#date').val();
      var time = $('#time').val();

      var value = moment(date + " / " + time, "DD MMM YYYY / HH : mm");
      var date_proposed = moment(value).format("YYYY-MM-DD hh:mm:ss");
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/schedule/mitra',
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        date_proposed: date_proposed
                    }
                });
      request.done(function(response) {
          $('#id').val('');
          $('#date').val('');
          $('#time').val('');
          $('#reschedule-modal').modal('hide');
          show_product();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function approve(id){
    document.getElementById("id-approve").value = id;
    document.getElementById("schedule").value = schedule;
    $("#approve-modal").modal('show');
  }

  function reschedule(id,id1){
    document.getElementById("id-schedule").value = id;
    document.getElementById("id-transaction").value = id;
    $("#reschedule-modal").modal('show');
  }
</script>