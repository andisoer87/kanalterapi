
<script>
  $(document).ready(function(){

    show_data();
    show_data1();
    show_data2();
    show_data3();
    
    function show_data(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/public/gigs',
          async : true,
          dataType : 'text',
          success : function(data){
              var text = data
              obj = JSON.parse(text);
              $('#count-package').html(obj.data.length);
          }
      });
    }
    function show_data1(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/0',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var text = data
              obj = JSON.parse(text);
              $('#count-order').html('0');
          }
      });
    }
    function show_data2(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/3',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var text = data
              obj = JSON.parse(text);
              $('#count-done').html('0');
          }
      });
    }
    function show_data3(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/schedule/admin',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var text = data
              obj = JSON.parse(text);
              $('#count-schedule').html(obj.data.length);
          }
      });
    }
  });
</script>