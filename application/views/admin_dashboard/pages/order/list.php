
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Paket</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- ./row -->
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-cyan card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Baru</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Terbayar</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Selesai</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-settings-tab" data-toggle="pill" href="#custom-tabs-one-settings" role="tab" aria-controls="custom-tabs-one-settings" aria-selected="false">Withdraw</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Detail customer
                                </th>
                                <th style="width: 15%">
                                    Nama paket
                                </th>
                                <th style="width: 15%">
                                    Jenis paket
                                </th>
                                <th>
                                    Total
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Status
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                        </thead>
                        <tbody id="order-display">
                            
                        </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Detail customer
                                </th>
                                <th style="width: 15%">
                                    Nama paket
                                </th>
                                <th style="width: 15%">
                                    Jenis paket
                                </th>
                                <th>
                                    Total
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Status
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                        </thead>
                        <tbody id="paid-display">
                            
                        </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Detail customer
                                </th>
                                <th style="width: 15%">
                                    Nama paket
                                </th>
                                <th style="width: 15%">
                                    Jenis paket
                                </th>
                                <th>
                                    Total
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Status
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                        </thead>
                        <tbody id="finish-display">
                            
                        </tbody>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-settings" role="tabpanel" aria-labelledby="custom-tabs-one-settings-tab">
                    <table class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Detail customer
                                </th>
                                <th style="width: 15%">
                                    Nama paket
                                </th>
                                <th style="width: 15%">
                                    Jenis paket
                                </th>
                                <th>
                                    Total
                                </th>
                                <th style="width: 8%" class="text-center">
                                    Status
                                </th>
                                <th style="width: 20%">
                                </th>
                            </tr>
                        </thead>
                        <tbody id="withdraw-display">
                            
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      <!-- /.modal -->
      <div class="modal fade" id="paid-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi pembayaran</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              Apakah pembayaran telah dilakukan dengan benar?
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" id="id-transaction" name="id-transaction">
                  <input type="hidden" id="id-gigs" name="id-gigs">
                  <input type="hidden" id="id-package" name="id-package">
                  <input type="hidden" id="id-email" name="id-email">
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a class="btn bg-default" data-dismiss="modal">Tidak</a>
              <a class="btn bg-cyan" id="btn_paid">Iya! Sudah terbayar</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- /.modal -->
      <div class="modal fade" id="finish-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi pelaksanaan</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              Apakah pelaksanaan terapi telah selesai dilakukan?
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" id="1id-transaction" name="1id-transaction">
                  <input type="hidden" id="1id-gigs" name="1id-gigs">
                  <input type="hidden" id="1id-package" name="1id-package">
                  <input type="hidden" id="1id-email" name="1id-email">
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a class="btn bg-default" data-dismiss="modal">Tidak</a>
              <a class="btn bg-cyan" id="btn_finish">Iya</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- /.modal -->
      <div class="modal fade" id="withdraw-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi withdraw</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              Apakah anda yakin akan melakukan withdraw
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" id="2id-transaction" name="2id-transaction">
                  <input type="hidden" id="2id-gigs" name="2id-gigs">
                  <input type="hidden" id="2id-package" name="2id-package">
                  <input type="hidden" id="2id-email" name="2id-email">
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a class="btn bg-default" data-dismiss="modal">Tidak</a>
              <a class="btn bg-cyan" id="btn_withdraw">Iya</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

