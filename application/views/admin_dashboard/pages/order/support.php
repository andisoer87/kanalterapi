<script>
  $(document).ready(function(){

    show_order();
    show_paid();
    show_finish();
    show_withdraw();
    
    function show_order(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/0',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<th>'+
                                '#'+
                            '</th>'+
                            '<td>'+
                                '<a>'+
                                  obj.data[i].user_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<th>'+
                                obj.data[i].gigs_name+
                            '</th>'+
                            '<td>'+
                                obj.data[i].gigs_package_name+
                            '</td>'+
                            '<th>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</th>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-danger">New</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<a class="btn btn-info text-white btn-sm" onclick="detailPaid(\'' + obj.data[i].transaction_id + '\',\'' + obj.data[i].gigs_id + '\',\'' + obj.data[i].gigs_package_id + '\',\''+obj.data[i].user_mail+'\')">'+
                                    '<i class="fas fa-pencil-alt">'+
                                    '</i> | Konfirmasi dibayar'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#order-display').html(html);
          }
      });
    }
    function show_paid(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/1',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<th>'+
                                '#'+
                            '</th>'+
                            '<td>'+
                                '<a>'+
                                  obj.data[i].user_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<th>'+
                                obj.data[i].gigs_name+
                            '</th>'+
                            '<td>'+
                                obj.data[i].gigs_package_name+
                            '</td>'+
                            '<th>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</th>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-danger">Paid</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<a class="btn btn-info text-white btn-sm" onclick="detailFinish(\'' + obj.data[i].transaction_id + '\',\'' + obj.data[i].gigs_id + '\',\'' + obj.data[i].gigs_package_id + '\',\''+obj.data[i].user_mail+'\')">'+
                                    '<i class="fas fa-pencil-alt">'+
                                    '</i> | Konfirmasi selesai'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#paid-display').html(html);
          }
      });
    }
    function show_finish(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/2',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<th>'+
                                '#'+
                            '</th>'+
                            '<td>'+
                                '<a>'+
                                  obj.data[i].user_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<th>'+
                                obj.data[i].gigs_name+
                            '</th>'+
                            '<td>'+
                                obj.data[i].gigs_package_name+
                            '</td>'+
                            '<th>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</th>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-danger">Finish</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<a class="btn btn-info text-white btn-sm" onclick="detailWithdraw(\'' + obj.data[i].transaction_id + '\',\'' + obj.data[i].gigs_id + '\',\'' + obj.data[i].gigs_package_id + '\',\''+obj.data[i].user_mail+'\')">'+
                                    '<i class="fas fa-pencil-alt">'+
                                    '</i> | Withdraw'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#finish-display').html(html);
          }
      });
    }
    function show_withdraw(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/admin/3',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<th>'+
                                '#'+
                            '</th>'+
                            '<td>'+
                                '<a>'+
                                  obj.data[i].user_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<th>'+
                                obj.data[i].gigs_name+
                            '</th>'+
                            '<td>'+
                                obj.data[i].gigs_package_name+
                            '</td>'+
                            '<th>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</th>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-danger">Withdraw</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<a class="btn btn-info text-white btn-sm">'+
                                    '<i class="fas fa-pencil-alt">'+
                                    '</i> | done'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#withdraw-display').html(html);
          }
      });
    }
    
    $('#btn_paid').on('click',function(){

      var id = $('#id-transaction').val();
      var id1 = $('#id-gigs').val();
      var id2 = $('#id-package').val();
      var email = $('#id-email').val();
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/order',
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        status_transaction: '1'
                    }
                });
      request.done(function(response) {
          $('#id-transaction').val('');
          $('#id-gigs').val('');
          $('#id-package').val('');
          $('#id-email').val('');
          $('#paid-modal').modal('hide');
          show_order();
          show_paid();
          window.location.href = "<?php echo base_url('Admin_order_control/approve/?transaction=') ?>" + id + "&gigs=" + id1 + "&package=" + id2 + "&email=" + email;
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
    $('#btn_finish').on('click',function(){

      var id = $('#1id-transaction').val();
      var id1 = $('#1id-gigs').val();
      var id2 = $('#1id-package').val();
      var email = $('#id-email').val();
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/order',
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        status_transaction: '2'
                    }
                });
      request.done(function(response) {
          $('#1id-transaction').val('');
          $('#1id-gigs').val('');
          $('#1id-package').val('');
          $('#1id-email').val('');
          $('#finish-modal').modal('hide');
          show_paid();
          show_finish();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
    $('#btn_withdraw').on('click',function(){

      var id = $('#2id-transaction').val();
      var id1 = $('#2id-gigs').val();
      var id2 = $('#2id-package').val();
      var email = $('#2id-email').val();
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/order',
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        status_transaction: '3'
                    }
                });
      request.done(function(response) {
          $('#2id-transaction').val('');
          $('#2id-gigs').val('');
          $('#2id-package').val('');
          $('#2id-email').val('');
          $('#withdraw-modal').modal('hide');
          show_finish();
          show_withdraw();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function detailPaid(id,id1,id2,email){
    document.getElementById("id-transaction").value = id;
    document.getElementById("id-gigs").value = id1;
    document.getElementById("id-package").value = id2;
    document.getElementById("id-email").value = email;
    $("#paid-modal").modal('show');
  }

  function detailFinish(id,id1,id2,email){
    document.getElementById("1id-transaction").value = id;
    document.getElementById("1id-gigs").value = id1;
    document.getElementById("1id-package").value = id2;
    document.getElementById("1id-email").value = email;
    $("#finish-modal").modal('show');
  }
  
  function detailWithdraw(id,id1,id2,email){
    document.getElementById("2id-transaction").value = id;
    document.getElementById("2id-gigs").value = id1;
    document.getElementById("2id-package").value = id2;
    document.getElementById("2id-email").value = email;
    $("#withdraw-modal").modal('show');
  }
</script>