
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data sub paket <span id="title"></span></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Paket</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <section class="content">
      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch" id="content-display">
            
          </div>
        </div>
      </div>
      <!-- /.card -->
      
      <!-- /.modal -->
      <div class="modal fade" id="edit-package">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit data paket</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              <div class="card-body">
                <div class="form-group">
                  <label for="input-name">Nama</label>
                  <input type="text" class="form-control" id="edit-name" name="name" placeholder="Nama paket">
                </div>
                <div class="form-group">
                  <label for="input-price">Harga</label>
                  <input type="text" class="form-control" id="edit-price" name="price" placeholder="Harga paket">
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" id="edit-desc" name="desc" placeholder="Isi deskripsi ..."></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <input type="submit" class="btn bg-cyan" value="Simpan">
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

