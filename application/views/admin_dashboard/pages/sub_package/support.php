<script>
  $(document).ready(function(){

    show_product();
    
    function show_product(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/gigs-package/<?php echo $id_package; ?>',
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var html1 = '<?php echo $name_package; ?>';
              var i;
              var text = data
              obj = JSON.parse(text);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;
                  html += '<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">'+
                            '<div class="card bg-light">'+
                                '<div class="card-header text-muted border-bottom-0">'+
                                html1+
                                '</div>'+
                                '<div class="card-body pt-0">'+
                                '<div class="row">'+
                                    '<div class="col-7">'+
                                    '<h2 class="lead"><b>'+obj.data[i].name+'</b></h2>'+
                                    '<p class="text-muted text-sm"><b>Harga: Rp. </b> '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")+' </p>'+
                                    '<ul class="ml-4 mb-0 fa-ul text-muted">'+
                                        '<li class="small"><span class="fa-li"><i class="fas fa-lg fa-book"></i></span> '+obj.data[i].desc+'</li>'+
                                    '</ul>'+
                                    '</div>'+
                                    '<div class="col-5 text-center">'+
                                    '<img src="<?php echo base_url(); ?>assets/dist/img/mental-health.jpg" alt="" class="img-circle img-fluid">'+
                                    '</div>'+
                                '</div>'+
                                '</div>'+
                                '<div class="card-footer">'+
                                '<div class="text-right">'+
                                    '<a href="#" title="Edit" class="btn btn-sm bg-teal">'+
                                    '<i class="fas fa-edit"></i>'+
                                    '</a> '+
                                    '<a href="#" title="Delete" class="btn btn-sm bg-danger">'+
                                    '<i class="fas fa-trash"></i>'+
                                    '</a>'+
                                '</div>'+
                                '</div>'+
                            '</div>'+
                          '</div>';
              }
              $('#content-display').html(html);
              $('#title').html(html1);
          }
      });
    }
    $('#btn_save').on('click',function(){
      
      var name  = $('#input-name').val();
      var price = $('#input-price').val();
      var desc  = $('#input-desc').val();
      var type  = '0';

      request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs-package/<?php echo $id_package; ?>',
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        name: name,
                        price: price,
                        desc: desc,
                        type: type
                    }
                });
      request.done(function(response) {
          $('#add-package').modal('hide');
          show_product();
          toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.');
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });
</script>