<script>
  $(document).ready(function(){

    show_product();
    
    function show_product(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/users/mitra',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<td>'+
                                '#'+
                            '</td>'+
                            '<td>'+
                                '<a>'
                                    +obj.data[i].kota+','+obj.data[i].provinsi+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].email+
                                '</small>'+
                            '</td>'+
                            '<td>'+
                                '<img height="70px" src="'+obj.data[i].photo+'"> '+obj.data[i].name+
                            '</td>'+
                            '<td>'+
                                obj.data[i].handphone +
                            '</td>'+
                            '<td class="project-state">'+
                                obj.data[i].date_created.substr(0,10)+' (pada jam '+obj.data[i].date_created.substr(11,5)+')'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<a class="btn btn-info btn-sm" href="#">'+
                                    'Curriculum Vitae <i class="bx bx-pencil">'+
                                    '</i>'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#mitra-display').html(html);
          }
      });
    }
  });
</script>