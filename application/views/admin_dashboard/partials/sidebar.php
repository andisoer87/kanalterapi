
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-cyan elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?php echo base_url(); ?>assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Kanalterapi</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image text-center">
          <img src="<?php echo base_url(); ?>assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('email'); ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_dashboard_control/index'); ?>" class="nav-link <?php if (@$page == "Dashboard") { echo "active"; } ?>">
              <i class="nav-icon bx bx-window-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_record_control/index'); ?>" class="nav-link <?php if (@$page == "Record") { echo "active"; } ?>">
              <i class="nav-icon bx bx-first-aid"></i>
              <p>
                Rekam Medis
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_mitra_control/index'); ?>" class="nav-link <?php if (@$page == "Mitra") { echo "active"; } ?>">
              <i class="nav-icon bx bx-user"></i>
              <p>
                Mitra
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_package_control/index'); ?>" class="nav-link <?php if (@$page == "Paket") { echo "active"; } ?>">
              <i class="nav-icon bx bx-package"></i>
              <p>
                Paket tersedia
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_order_control/index'); ?>" class="nav-link <?php if (@$page == "Order") { echo "active"; } ?>">
              <i class="nav-icon bx bx-task"></i>
              <p>
                Order
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url('index.php/admin_schedule_control/index'); ?>" class="nav-link <?php if (@$page == "Jadwal") { echo "active"; } ?>">
              <i class="nav-icon bx bx-calendar"></i>
              <p>
                Jadwal
              </p>
            </a>
          </li>
          <!-- <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data v3</p>
                </a>
              </li>
            </ul>
          </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
