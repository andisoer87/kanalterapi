<div style="padding-top: 100px"></div>

<div class="col-12 container res-p-4" id="main-content" style="margin-bottom: 20px; min-height: calc(100% - 340px);">

    <!-- ----- TITLE ----- -->
    <h1 class="title-medium color-black">Layanan yang di order</h1>

    <div class="mt-4 d-flex overflow-auto">
        <button class="tablink" onclick="openTab('Baru', this)" id="defaultOpen">Baru</button>
        <button class="tablink" onclick="openTab('Pembayaran', this)">Pembayaran</button>
        <button class="tablink" onclick="openTab('Penjadwalan', this)">Penjadwalan</button>
        <button class="tablink" onclick="openTab('Selesai', this)">Selesai</button>
    </div>

    <div id="Baru" class="col-12 m-0 p-0 tabcontent overflow-auto">
        <table class="table col-12 m-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Layanan</th>
                    <th scope="col">Paket</th>
                    <th scope="col">Status Transaksi</th>
                    <th scope="col">Harga</th>
                </tr>
            </thead>
            <tbody id="orderList">
            </tbody>
        </table>
    </div>

    <div id="Pembayaran" class="col-12 m-0 p-0 tabcontent overflow-auto">
        <table class="table col-12 m-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Layanan</th>
                    <th scope="col">Paket</th>
                    <th scope="col">Status Transaksi</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody id="orderPembayaranList">
            </tbody>
        </table>
    </div>

    <div id="Penjadwalan" class="col-12 m-0 p-0 tabcontent overflow-auto">
        <div class="d-flex overflow-auto">
            <button class="tablink2" onclick="openTab2('Layanan', this)" id="defaultOpen2">Layanan</button>
            <button class="tablink2" onclick="openTab2('Kelas', this)">Kelas</button>
        </div>
        <div id="Layanan" class="col-12 m-0 p-0 tabcontent2 overflow-auto">
            <table class="table col-12 m-0">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Layanan</th>
                        <th scope="col">Paket</th>
                        <th scope="col">Status</th>
                        <th scope="col">Harga</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody id="orderPenjadwalanList">
                </tbody>
            </table>
        </div>
        <div id="Kelas" class="col-12 m-0 p-0 tabcontent2 overflow-auto">
            <table class="table col-12 m-0">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nama Layanan</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Kadaluarsa</th>
                        <th scope="col">Keterangan</th>
                    </tr>
                </thead>
                <tbody id="orderClassroom">
                </tbody>
            </table>
        </div>
    </div>

    <div id="Selesai" class="col-12 m-0 p-0 tabcontent overflow-auto">
        <table class="table col-12 m-0">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Layanan</th>
                    <th scope="col">Paket</th>
                    <th scope="col">Harga</th>
                </tr>
            </thead>
            <tbody id="orderSelesaiList">
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="Rating" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="ratingForm">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Selesaikan Pesanan</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="paragraph-small">Sebelum menyelaikan pesanan, harap anda memberikan tanggapan tentang layanan ini : </p>
                        <div class="mt-4">
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Kualitas</label>
                                <div class="rating">
                                    <label class="form-check form-check-inline mr-0">
                                        <input type="radio" value="1" name="star">
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                    </label>
                                    <label class="form-check form-check-inline mr-0">
                                        <input type="radio" value="2" name="star">
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                    </label>
                                    <label class="form-check form-check-inline mr-0">
                                        <input type="radio" value="3" name="star">
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                    </label>
                                    <label class="form-check form-check-inline mr-0">
                                        <input type="radio" value="4" name="star">
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                    </label>
                                    <label class="form-check form-check-inline mr-0">
                                        <input type="radio" value="4" name="star">
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                        <i class='bx bxs-star' style="font-size: 28px"></i>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Tanggapan</label>
                                <textarea class="form-control paragraph-small" name="tanggapan" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a type="button" class="btn btn-light color-black" data-dismiss="modal">Kembali</a>
                        <button type="submit" class="btn btn-primary color-white">Selesaikan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function openTab(cityName, elmnt) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
            tablinks[i].style.color = '';
        }
        document.getElementById(cityName).style.display = "block";
        elmnt.style.backgroundColor = '#1ea5a8';
        elmnt.style.color = '#fff';

    }
    function openTab2(cityName, elmnt) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent2");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink2");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.color = '';
            tablinks[i].style.fontWeight = 'normal';
            tablinks[i].style.borderBottomWidth = "0px";
        }
        document.getElementById(cityName).style.display = "block";
        elmnt.style.color = '#1ea5a8';
        elmnt.style.fontWeight = 'bold';
        elmnt.style.borderBottomWidth = "2px";

    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
    document.getElementById("defaultOpen2").click();
</script>

<script>
    var request;
    var requestTransfer;
    var requestSchedule;
    var requestSelesai;
    var requestClass;
    var requestNested;
    var requestCheckClass;
    var requestJoin;
    var putSchedule;

    var orderData;
    var orderDataTransfer = [];
    var orderDataSchedule;
    var orderSelesai;
    var orderClass;

    var selectedGigsId;
    var selectedTransactionId;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        requestData();
    });

    function resetTable() {
        $("#orderList").empty();
        $("#orderPembayaranList").empty();
        $("#orderSelesaiList").empty();
        $("#orderPenjadwalanList").empty();
        $("#orderClassroom").empty();
    }

    function requestData() {
        resetTable();
        if (request || requestSchedule || requestSelesai || requestTransfer) {
            request.abort();
            requestSchedule.abort();
            requestSelesai.abort();
        }

        request = $.ajax({
            url: 'http://103.129.223.136:2099/order/customer/0',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            orderData = response.data;
            orderDataTransfer = response.data;

            console.log(response);

            if (success) {
                if (orderData.length == 0) {
                    $("#orderList").append(`
                        <tr>
                            <td scope="row" class="text-center" colspan="5">tidak ada data</td>
                        </tr>
                    `);
                }
                orderData.reverse();
                for (i = 0; i < orderData.length; i++) {
                    var value = orderData[i];
                    var nowa = "6283834456733";
                    var watext = `*Konfirmasi pembayaran*%0AKonfirmasi pembayaran kanalterapi.com,%0A%0ANama : ` + userData[0].name + `%0ATerapi : ` + value.gigs_name + `%0AJumlah Bayar : Rp ` + numberWithCommas(value.price) + `%0A%0A` + moment().format('d MMMM YYYY') + `%0A_*sertakan juga foto bukti transfer_`;
                    $("#orderList").append(`
                        <tr>
                            <th scope="row">` + (i + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + value.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + value.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + value.gigs_name + `</p></td>
                            <td>` + value.gigs_package_name + `</td>
                            <td><span class="badge badge-light pt-2 pb-2 pl-3 pr-3">belum dibayar</span></td>
                            <td>Rp ` + numberWithCommas(value.price) + `</td>
                        </tr>
                    `);
                    $("#orderPembayaranList").append(`
                        <tr>
                            <th scope="row">` + (i + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + value.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + value.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + value.gigs_name + `</p></td>
                            <td>` + value.gigs_package_name + `</td>
                            <td><span class="badge badge-warning pt-2 pb-2 pl-3 pr-3">belum dibayar</span></td>
                            <td>Rp ` + numberWithCommas(value.price) + `</td>
                            <td><a data-toggle="tooltip" data-placement="top" title="Tambah Bukti Pembayaran" href="https://api.whatsapp.com/send?phone=` + nowa + `&text=` + watext + `"><i class='bx bxs-camera-plus color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a></td>
                        </tr>
                    `);
                }
                $(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                })

                requestTransfer = $.ajax({
                    url: 'http://103.129.223.136:2099/order/customer/1',
                    type: 'get',
                    headers: {
                        'access-token': '<?php echo $this->session->userdata('token'); ?>'
                    },
                    data: {

                    }
                });

                requestTransfer.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    orderDataTransfer = [].concat(orderDataTransfer, response.data);

                    console.log("Lunas", response);
                    console.log("data", orderDataTransfer);

                    if (success) {
                        if (orderDataTransfer.length == 0) {
                            $("#orderPembayaranList").append(`
                                <tr>
                                    <td scope="row" class="text-center" colspan="5">tidak ada data</td>
                                </tr>
                            `);
                        }
                        for (var i = orderData.length; i < orderDataTransfer.length; i++) {
                            var valueDataTransfer = orderDataTransfer[i];

                            getTransaction(valueDataTransfer, i);
                        }
                        $(function() {
                            $('[data-toggle="tooltip"]').tooltip();
                        })
                    } else {
                        console.error(response);
                    }
                });

                requestTransfer.fail(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.error(response);
                });
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestSelesai = $.ajax({
            url: 'http://103.129.223.136:2099/order/customer/3',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        requestSelesai.done(function(response) {
            var success = response.success;
            var message = response.message;
            orderSelesai = response.data;

            console.log(response);

            if (success) {
                if (orderSelesai.length == 0) {
                    $("#orderSelesaiList").append(`
                        <tr>
                            <td scope="row" class="text-center" colspan="4">tidak ada data</td>
                        </tr>
                    `);
                }
                orderSelesai.reverse();
                for (i = 0; i < orderSelesai.length; i++) {
                    var value = orderSelesai[i];

                    $("#orderSelesaiList").append(`
                        <tr>
                            <th scope="row">` + (i + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + value.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + value.image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + value.name + `</p></td>
                            <td>` + value.package_name + `</td>
                            <td>Rp ` + numberWithCommas(value.price) + `</td>
                        </tr>
                    `);
                }

                $(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            } else {
                console.error(response);
            }
        });

        requestSelesai.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestSchedule = $.ajax({
            url: 'http://103.129.223.136:2099/schedule/customer',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        requestSchedule.done(function(response) {
            var success = response.success;
            var message = response.message;
            orderDataSchedule = response.data;

            console.log(response);

            if (success) {
                if (orderDataSchedule.length == 0) {
                    $("#orderPenjadwalanList").append(`
                        <tr>
                            <td scope="row" class="text-center" colspan="7">tidak ada data</td>
                        </tr>
                    `);
                }
                orderDataSchedule = orderDataSchedule.filter(function (item, index) {
                    var nextIndex = index + 1;
                    return orderDataSchedule[nextIndex] == null || orderDataSchedule[index].transaction_id != orderDataSchedule[nextIndex].transaction_id
                });
                orderDataSchedule.reverse();
                for (i = 0; i < orderDataSchedule.length; i++) {
                    var value = orderDataSchedule[i];

                    var status;
                    var statusColor = '';
                    var statusType = 0;
                    if (value.status_customer == 1 && value.status_mitra == 1) {
                        status = 'Jadwal Telah Diatur';
                        statusColor = 'success';
                        statusType = 3;
                    } else if (value.status_customer == 1 && value.status_mitra == 0) {
                        status = 'Menunggu Konfirmasi Terapis';
                        statusColor = 'light';
                        statusType = 2;
                    } else if (value.status_customer == 0 && value.status_mitra == 1) {
                        status = 'Menunggu Konfirmasi Anda';
                        statusColor = 'warning';
                        statusType = 1;
                    } else {
                        status = 'tidak diketahui';
                        statusColor = 'danger';
                        statusType = 0;
                    }
                    
                    var action = '';
                    if (statusType == 3) {
                        action = `<a data-toggle="tooltip" data-placement="top" title="Selesaikan Pesanan" onclick="selesaikan('` + value.gigs_id + `', '` + value.transaction_id + `')"><i class='bx bxs-chevrons-right color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>`;
                    } else if (statusType == 2) {
                        action = '-';
                    } else if (statusType == 1) {
                        action = `<a data-toggle="tooltip" data-placement="top" title="Ubah Jadwal" target="_blank" href="<?php echo base_url('schedule') ?>?transaction=` + value.transaction_id + `&gigs=` + value.gigs_id + `&package=` + value.gigs_package_id + `"><i class='bx bxs-pencil color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>
                                <a data-toggle="tooltip" data-placement="top" title="Setujui Jadwal" onclick="approveschedule( '` + value.schedule_id + `', '` + value.transaction_id + `', '1' )"><i class='bx bxs-paper-plane color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>`;
                    } else {
                        action = '?';
                    }


                    $("#orderPenjadwalanList").append(`
                        <tr>
                            <th scope="row">` + (i + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + value.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + value.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px">
                                <p class="paragraph-medium">` + value.gigs_name + `</p>
                            </td>
                            <td>` + value.gigs_package_name + `</td>
                            <td><span class="badge badge-` + statusColor + ` pt-2 pb-2 pl-3 pr-3">` + status + `</span></td>
                            <td>Rp ` + numberWithCommas(value.price) + `</td>
                            <td>` + moment(value.date_proposed).format('D MMM YYYY - HH:mm') + `</td>
                            <td>` + action + `</td>
                        </tr>
                    `);
                }

                $(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            } else {
                console.error(response);
            }
        });

        requestSchedule.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestClass = $.ajax({
            url: 'http://103.129.223.136:2099/classroom/customer',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        requestClass.done(function(response) {
            var success = response.success;
            var message = response.message;
            orderClass = response.data;

            console.log(response);

            if (success) {
                if (orderClass.length == 0) {
                    $("#orderClassroom").append(`
                        <tr>
                            <td scope="row" class="text-center" colspan="7">tidak ada data</td>
                        </tr>
                    `);
                }
                orderClass.reverse();
                for (i = 0; i < orderClass.length; i++) {
                    var value = orderClass[i];

                    $("#orderClassroom").append(`
                        <tr>
                            <th scope="row">` + (i + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + value.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + value.gigs_image +`" class="mr-3" style="width: 100px; border-radius: 8px">
                                <p class="paragraph-medium">` + value.gigs_name + `</p>
                            </td>
                            <td>` + moment(value.date_room).format('D MMM YYYY - HH:mm') + `</td>
                            <td>` + moment(value.date_expired).format('D MMM YYYY - HH:mm') + `</td>
                            <td>` + value.gigs_desc + `</td>
                        </tr>
                    `);
                }

                $(function() {
                    $('[data-toggle="tooltip"]').tooltip();
                })
            } else {
                console.error(response);
            }
        });

        requestClass.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    }

    function getTransaction(valueDataTransfer, index) {
        requestNested = $.ajax({
            url: 'http://103.129.223.136:2099/schedule/customer',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        requestNested.done(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.log("p",response);

            if (success) {

                var scheduleData = data.filter(function(val) {
                    return val.transaction_id == valueDataTransfer.transaction_id
                });

                if (scheduleData.length == 0) {
                    if (valueDataTransfer.gigs_type == 1) {
                        checkClass(valueDataTransfer, index);
                    } else {
                        $("#orderPembayaranList").append(`
                            <tr>
                                <th scope="row">` + (index + 1) + `</th>
                                <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + valueDataTransfer.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + valueDataTransfer.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + valueDataTransfer.gigs_name + `</p></td>
                                <td>` + valueDataTransfer.gigs_package_name + `</td>
                                <td><span class="badge badge-success pt-2 pb-2 pl-3 pr-3">sudah dibayar</span></td>
                                <td>Rp ` + numberWithCommas(valueDataTransfer.price) + `</td>
                                <td><a data-toggle="tooltip" data-placement="top" title="Tambah Jadwal" href="<?php echo base_url('schedule') ?>?transaction=` + valueDataTransfer.transaction_id + `&gigs=` + valueDataTransfer.gigs_id + `&package=` + valueDataTransfer.gigs_package_id + `"><i class='bx bx-calendar-plus color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a></td>
                            </tr>
                        `);
                    }
                } else {
                    $("#orderPembayaranList").append(`
                        <tr>
                            <th scope="row">` + (index + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + valueDataTransfer.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + valueDataTransfer.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + valueDataTransfer.gigs_name + `</p></td>
                            <td>` + valueDataTransfer.gigs_package_name + `</td>
                            <td><span class="badge badge-success pt-2 pb-2 pl-3 pr-3">sudah dibayar</span></td>
                            <td>Rp ` + numberWithCommas(valueDataTransfer.price) + `</td>
                            <td>-</td>
                        </tr>
                    `);
                }
            } else {
                console.error(response);
            }
        });

        requestNested.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    }

    function checkClass(valueDataTransfer, index) {
        requestCheckClass = $.ajax({
            url: 'http://103.129.223.136:2099/classroom/customer',
            type: 'get',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {

            }
        });

        requestCheckClass.done(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            if (success) {
                var classData = data.filter(function(val) {
                    return val.transaction_id == valueDataTransfer.transaction_id
                });

                if (classData.length > 0) {
                    $("#orderPembayaranList").append(`
                        <tr>
                            <th scope="row">` + (index + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + valueDataTransfer.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + valueDataTransfer.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + valueDataTransfer.gigs_name + `</p></td>
                            <td>` + valueDataTransfer.gigs_package_name + `</td>
                            <td><span class="badge badge-success pt-2 pb-2 pl-3 pr-3">sudah dibayar</span></td>
                            <td>Rp ` + numberWithCommas(valueDataTransfer.price) + `</td>
                            <td><a data-toggle="tooltip" data-placement="top" title="Gabung Kelas" onclick="joinClass('` + valueDataTransfer.transaction_id + `', '` + valueDataTransfer.gigs_id + `')"><i class='bx bxs-door-open color-white bg-primary' style="padding: 12px; border-radius: 8px; cursor: pointer"></i></a></td>
                        </tr>
                    `);
                } else {
                    $("#orderPembayaranList").append(`
                        <tr>
                            <th scope="row">` + (index + 1) + `</th>
                            <td onclick="window.open('<?php echo base_url('detail_layanan?gigs=') ?>` + valueDataTransfer.gigs_id + `', '_blank')" style="cursor: pointer;" class="d-flex"><img src="<?php echo base_url('assets/package/master/') ?>` + valueDataTransfer.gigs_image + `" class="mr-3" style="width: 100px; border-radius: 8px"><p class="paragraph-medium">` + valueDataTransfer.gigs_name + `</p></td>
                            <td>` + valueDataTransfer.gigs_package_name + `</td>
                            <td><span class="badge badge-success pt-2 pb-2 pl-3 pr-3">sudah dibayar</span></td>
                            <td>Rp ` + numberWithCommas(valueDataTransfer.price) + `</td>
                            <td>-</td>
                        </tr>
                    `);
                }
            } else {
                console.error(response);
            }
        });

        requestCheckClass.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    }

    function approveschedule(schedule_id, transaction_id, status) {
        if (putSchedule) {
            putSchedule.abort();
        }

        putSchedule = $.ajax({
            url: 'http://103.129.223.136:2099/schedule/customer',
            type: 'put',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {
                schedule_id: schedule_id,
                transaction_id: transaction_id,
                status: status
            }
        });

        putSchedule.done(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.log(response);

            if (success) {
                alert(message);
            } else {
                console.error(response);
            }
            requestData();
        });

        putSchedule.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    }

    function selesaikan(gigsId, transactionId) {
        selectedGigsId = gigsId;
        selectedTransactionId = transactionId;
        $("input[name='star']").prop( "checked", false );
        $("textarea[name='tanggapan']").val("");
        $('#Rating').modal('show');
    }

    function joinClass(transaction_id, gigs_id) {
        if (requestJoin) {
            requestJoin.abort();
        }

        requestJoin = $.ajax({
            url: 'http://103.129.223.136:2099/classroom/customer',
            type: 'post',
            headers: {
                'access-token': '<?php echo $this->session->userdata('token'); ?>'
            },
            data: {
                transaction_id: transaction_id,
                gigs_id: gigs_id
            }
        });

        requestJoin.done(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.log(response);

            if (success) {
                alert(message);
            } else {
                console.error(response);
                alert(message);
            }
            requestData();
        });

        requestJoin.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    }

    $("#ratingForm").submit(function(event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }

        var starValue = $("input[name='star']:checked").val();
        var tanggapan = $("textarea[name='tanggapan']").val();

        if ('<?php echo $this->session->userdata('token'); ?>' != '' && selectedGigsId != null && selectedTransactionId != null && starValue != null) {
            request = $.ajax({
                url: 'http://103.129.223.136:2099/gigs-rate/' + selectedGigsId,
                type: 'post',
                headers: {
                    'access-token': '<?php echo $this->session->userdata('token'); ?>'
                },
                data: {
                    transaction_id: selectedTransactionId,
                    comment: tanggapan,
                    rating: starValue
                }
            });

            request.done(function(response) {
                var success = response.success;
                var message = response.message;
                gigsPackage = response.data;

                console.log(response);

                if (success) {
                    alert("Berhasil : " + message);
                    $('#Rating').modal('hide');
                } else {
                    console.error(response);
                    alert("Gagal : " + message);
                    $('#Rating').modal('hide');
                }
                requestData();
            });

            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;

                alert("Gagal : " + message);
                $('#Rating').modal('hide');
            });
        }
    });

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    })
</script>