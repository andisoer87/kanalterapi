<div style="padding-top: 100px"></div>
<div class="res-p-4" style=" min-height: calc(100% - 320px);">
    <div class="col-12 container row mb-4">
        <!-- ----- TITLE ----- -->
        <div class="col-12 d-flex align-items-end justify-content-between p-0 mt-2">
            <h2 class="color-primary">Semua Terapis</h2>
        </div>

        <hr class="col-12 row mt-1 mb-3 mr-0 ml-0 pl-0 pr-0">

        <div class="col-12 container row grid-3" id="gigsListLayout">
        </div>
    </div>
</div>

<script>
    var request;
    var nestedrequest;
    var gigsData;
    var value;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request) {
            request.abort();
        }

        request = $.ajax({
            url: 'http://103.129.223.136:2099/users/mitra',
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsData = response.data;

            console.log(response);

            if (success) {
                for (i = 0; i < gigsData.length; i++) {
                    value = gigsData[i];

                    $("#gigsListLayout").append(`
                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.id + `">
                        <div class="col-12 card-transparent p-4 d-flex flex-column align-items-center">
                            <img id="profile" class="img-profile mt-4" style="width: 120px; height: 120px" src="` +  value.photo + `">
                            <h3 class="mt-4">` + value.name + `</h3>

                            <hr class="w-100" style="border: #393939 solid 1px; opacity: .08; margin: 32px auto">

                            <div class="w-100 d-flex justify-content-between">
                                <p class="paragraph-medium">Asal</p>
                                <p class="paragraph-medium">` + value.kota + ', ' + value.provinsi + `</p>
                            </div>

                            <div class="w-100 d-flex justify-content-between mt-2 mb-2">
                                <p class="paragraph-medium">Bergabung</p>
                                <p class="paragraph-medium">` + moment(value.date_created).format("MMMM YYYY") + `</p>
                            </div>
                        </div>
                    </a>
                    `);
                }
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });
</script>