<!-- ----- TOP SECTION ----- -->
<div class="row w-100 h-100 m-0">
    <div class="col-12 h-100 d-flex flex-column p-lg-0 res-p-4" style="padding-top: 100px">

        <!-- ----- TITLE ----- -->
        <div class="container row flex-grow-1 align-items-center mt-sm-0 mt-100" style="margin-bottom: 120px;">
            <div class="col-lg-6 p-0 pr-lg-3">
                <h1 class="title-big">Cari Jasa Terapis Terbaik</h1>
                <h1 class="title-medium d-none d-lg-block d-md-block">Untuk Kesehatan Mentalmu</h1>

                <!-- ----- SEARCH ----- -->
                <form class="col-lg-10 row mt-4">
                    <div class="input-group flex-nowrap">
                        <input type="text" class="form-control pt-4 pb-4 pl-4" placeholder="Cari jasa terapi" style="border: none; border-radius: 12px 0px 0px 12px;">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-primary" style="border: none; border-radius: 0px 12px 12px 0px; padding: 0px 40px;">Cari</button>
                        </div>

                    </div>
                </form>
            </div>

            <!-- ----- MAIN IMAGE ----- -->
            <img class="col-lg-6 p-0" src="<?php echo base_url('assets/'); ?>img/therapy.png">
        </div>

        <!-- ----- BACKGROUND ----- -->
        <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1366 727.484" preserveAspectRatio="none" style="position: absolute; left: 0; right: 0; top: 0px; bottom: 0; z-index: -1;">
            <defs>
                <style>
                    .cls-1 {
                        fill: #f3f3f3;
                    }
                </style>
            </defs>
            <path id="Path_3" data-name="Path 3" class="cls-1" d="M1369,3238c-11.039,7.885-116.526,79.616-372,67s-361.976-164.423-662-166S3,3181,3,3181V2579H1369Z" transform="translate(-0 -2592)" />
        </svg>
    </div>
</div>


<!-- ----- FEATURE SECTION ----- -->
<div class="col-12 container row grid-3 res-p-4" style="margin-top: -80px;">
    <!-- ----- FEATURE 1 ----- -->
    <div class="card-transparent">
        <div class="d-flex align-items-center">
            <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                <i class='bx bx-badge-check bx-sm color-white'></i>
            </div>
            <h3>Pelayanan Nyaman</h3>
        </div>

        <p class="paragraph-small mt-3">Kami menyediakan layanan jasa terapi yang nyaman, mudah, dan terpercaya.</p>
    </div>

    <!-- ----- FEATURE 2 ----- -->
    <div class="card-transparent">
        <div class="d-flex align-items-center">
            <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                <i class='bx bx-donate-blood bx-sm color-white'></i>
            </div>
            <h3>Berbagai Jenis Terapi</h3>
        </div>

        <p class="paragraph-small mt-3">Kami menyediakan berbagai layanan jenis terapi. Mulai dari Cognitive
            Behavioral Therapy dan berbagai macam jenis terapi lainnya.</p>
    </div>

    <!-- ----- FEATURE 3 ----- -->
    <div class="card-transparent grid-3-child-wide">
        <div class="d-flex align-items-center">
            <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                <i class='bx bx-user bx-sm color-white'></i>
            </div>
            <h3>Terapis Professional</h3>
        </div>

        <p class="paragraph-small mt-3">Anda akan dilayani oleh terapis professional dan sudah berpengalaman.</p>
    </div>
</div>

<!-- ----- CLASS SECTION ----- -->
<div id="classGigsContainer" class="col-12 container row mt-4 mb-4 res-p-4">
    <!-- ----- TITLE ----- -->
    <div class="col-12 d-flex align-items-end justify-content-between p-0 mt-2">
        <h2 class="color-primary">Layanan Kelas Terbaru</h2>
        <a href="<?php echo base_url('SemuaLayanan') ?>" type="button" class="btn btn-outline-primary m-0 pl-4 pr-4 d-none d-lg-block d-md-block" style="font-size: 14px;">Lainnya</a>
    </div>

    <hr class="col-12 row mt-1 mb-3 mr-0 ml-0 pl-0 pr-0">

    <div class="col-12 container row slide-container">
        <div id="slide-item-container" style="width: 100%">

        </div>

        <a class="slide-prev" onclick="moveSlide(-1)">&#10094;</a>
        <a class="slide-next" onclick="moveSlide(1)">&#10095;</a>

        <div class="slide-indicator" id="slide-item-indicator">
            
        </div>
    </div>
</div>

<!-- ----- RECOMENDED SECTION ----- -->
<div class="col-12 container row mt-4 mb-4 res-p-4">
    <!-- ----- TITLE ----- -->
    <div class="col-12 d-flex align-items-end justify-content-between p-0 mt-2">
        <h2 class="color-primary">Rekomendasi Layanan</h2>
        <a href="<?php echo base_url('SemuaLayanan') ?>" type="button" class="btn btn-outline-primary m-0 pl-4 pr-4 d-none d-lg-block d-md-block" style="font-size: 14px;">Lainnya</a>
    </div>

    <hr class="col-12 row mt-1 mb-3 mr-0 ml-0 pl-0 pr-0">

    <div class="col-12 container row grid-3" id="gigsListLayout">
    </div>
    <a href="<?php echo base_url('SemuaLayanan') ?>" type="button" class="btn btn-outline-primary w-100 m-0 mt-4 pl-4 pr-4 pt-3 pb-3 d-block d-lg-none d-md-none" style="font-size: 14px;">Lainnya</a>
</div>

<!-- ----- CATEGORY SECTION ----- -->
<div class="col-12 container row mt-4 res-p-4">
    <!-- ----- TITLE ----- -->
    <div class="col-12 d-flex align-items-end justify-content-between p-0 mt-4 mb-4">
        <h2 class="color-primary">Kategori Layanan Kami</h2>
    </div>

    <div class="col-12 container row grid-3">
        <!-- ----- FEATURE 1 ----- -->
        <div class="card-transparent">
            <div class="d-flex align-items-center">
                <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                    <i class='bx bx-donate-blood bx-sm color-white'></i>
                </div>
                <h4>Cognitive Behavioral Therapy </h4>
            </div>
        </div>

        <!-- ----- FEATURE 2 ----- -->
        <div class="card-transparent">
            <div class="d-flex align-items-center">
                <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                    <i class='bx bx-donate-blood bx-sm color-white'></i>
                </div>
                <h4>Dialectical Behavioral Therapy</h4>
            </div>
        </div>

        <!-- ----- FEATURE 3 ----- -->
        <div class="card-transparent grid-3-child-wide">
            <div class="d-flex align-items-center">
                <div class="bg-primary d-flex align-items-center justify-content-center mr-3" style="border-radius: 12px; width: 40px; height: 40px;">
                    <i class='bx bx-donate-blood bx-sm color-white'></i>
                </div>
                <h4>Psychodynamic Therapy</h4>
            </div>
        </div>
    </div>
</div>

<!-- ----- SUB CONTENT SECTION ----- -->
<div class="col-12 container row mt-4 mb-4 pt-4 res-p-4">
    <img src="<?php echo base_url('assets/'); ?>img/terapis.png" class="col-lg-6 row mb-4 mb-lg-0 mb-sm-0" style="object-fit: scale-down;">
    <div class="col-lg-6 row pl-lg-4 ml-lg-4 mt-md-4 pt-md-4 mt-sm-4 pt-sm-4 justify-content-center" style="flex-direction: column;">
        <h1>Pelayanan Terjamin</h1>

        <h3 class="mt-4 pt-2 mb-2">Dilayani Oleh Terapis Professional</h3>
        <p class="paragraph-small">Kami menyediakan terapis professional dan berpengalaman. Layanan kami dapat
            dipastikan memiliki terapis yang melayani anda dengan nyaman dan aman.</p>

        <h3 class="mt-4 pt-2 mb-2">Transaksi Aman Terjaga</h3>
        <p class="paragraph-small">Kami melindungi penuh atas transaksi anda dengan terapis. Dan kami sudah
            memastikan bahwa terapis yang kami miliki tidak menipu dalam bentuk apapun.</p>
    </div>
</div>

<!-- ----- CLICK TO ACTION SECTION ----- -->
<div class="res-p-4 d-none d-lg-block d-md-block">
    <div class="col-12 container row justify-content-between mt-4 mb-4 p-4 br-12" style="background-color: rgba(30, 165, 168, .08);">
        <div>
            <h2 class="color-primary">Tertarik menjadi terapis?</h2>
            <p class="paragraph-small color-primary">Bergabunglah dengan kami!</p>
        </div>
        <a href="<?php echo base_url('mitra_register') ?>" class="btn btn-primary pl-4 pr-4">Gabung</a>
    </div>
</div>


<script>
    var activeSlide = 0;

    function moveSlide(x) {
        activeSlide += x;
        showSlide(activeSlide);
    }

    function currentSlide(x) {
        activeSlide = x;
        showSlide(activeSlide);
    }

    function showSlide(x) {
        var i;
        var slides = document.getElementsByClassName("slide-item");
        var dots = document.getElementsByClassName("slide-dot");

        if (slides.length > 0) {
            if (x > slides.length - 1) {activeSlide = 0}
            if (x < 0) {activeSlide = slides.length - 1}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[activeSlide].style.display = "block";
            dots[activeSlide].className += " active";
        }
    }
</script>

<script>
    var request;
    var nestedrequest;
    var nestedrequest2;
    var gigsData;
    var value;

    var requestClass;
    var classData;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request) {
            request.abort();
        }

        request = $.ajax({
            url: 'http://103.129.223.136:2099/public/gigs/0',
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsData = response.data;

            console.log(response);

            if (success) {
                gigsData.reverse();
                for (i = 0; i < gigsData.length && i < 6; i++) {
                    value = gigsData[i];

                    nestedrequest = $.ajax({
                        url: 'http://103.129.223.136:2099/gigs-package/' + value.id,
                        type: 'get',
                        data: {

                        }
                    });

                    nestedrequest.done(function(response) {
                        var success = response.success;
                        var message = response.message;
                        var data = response.data;

                        console.log(response);

                        if (success) {
                            if (data[0] && data[data.length - 1]) {
                                var lowprice = data[0].price;
                                var highprice = data[data.length - 1].price;
                                var priceGigs = "Rp " + numberWithCommas(lowprice) + " - Rp " + numberWithCommas(highprice);

                                $("#priceGigs-" + data[0].gigs_id).append(priceGigs);
                            }
                        } else {
                            console.error(response);
                        }
                    });

                    nestedrequest.fail(function(response) {
                        console.error(response);
                    });

                    if (i == 6) {
                        $("#gigsListLayout").append(`
                            <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + value.id + `'" style="cursor: pointer;" tabindex="1" class="card-item d-flex flex-column grid-3-child-hide">
                                <img src="<?php echo base_url('assets/package/master/'); ?>` + value.image + `" class="br-12 w-100" style="height: 220px; object-fit: cover;">
                                <div class="d-flex align-items-start justify-content-between card-item-title" style="flex: 1;">
                                    <div>
                                        <h3 class="mt-2">` + value.name + `</h3>
                                        <p class="paragraph-small mt-2">` + value.mitra_name + `</p>
                                    </div>
                                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.mitra_id + `"><img src="` + value.mitra_photo + `" class="img-profile"></a>
                                </div>
                                <div class="d-flex align-items-center justify-content-between card-item-price">
                                    <div class="d-flex align-items-center">
                                        <i class='bx bxs-star color-primary mr-1' style="font-size: 24px;"></i>
                                        <p class="paragraph-small color-primary" id="ratingGigs-` + value.id + `"></p>
                                    </div>
                                    <p class="paragraph-small color-primary"><strong id="priceGigs-` + value.id + `"></strong></p>
                                </div>
                            </div>
                        `);
                    } else {
                        $("#gigsListLayout").append(`
                            <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + value.id + `'" style="cursor: pointer;" tabindex="1" class="card-item d-flex flex-column">
                                <img src="<?php echo base_url('assets/package/master/'); ?>` + value.image + `" class="br-12 w-100" style="height: 220px; object-fit: cover;">
                                <div class="d-flex align-items-start justify-content-between card-item-title" style="flex: 1;">
                                    <div>
                                        <h3 class="mt-2">` + value.name + `</h3>
                                        <p class="paragraph-small mt-2">` + value.mitra_name + `</p>
                                    </div>
                                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.mitra_id + `"><img src="` + value.mitra_photo + `" class="img-profile"></a>
                                </div>
                                <div class="d-flex align-items-center justify-content-between card-item-price">
                                    <div class="d-flex align-items-center">
                                        <i class='bx bxs-star color-primary mr-1' style="font-size: 24px;"></i>
                                        <p class="paragraph-small color-primary" id="ratingGigs-` + value.id + `"></p>
                                    </div>
                                    <p class="paragraph-small color-primary"><strong id="priceGigs-` + value.id + `"></strong></p>
                                </div>
                            </div>
                        `);
                    }
                }
                nestedrequest2 = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs-rate-averages',
                    type: 'get',
                    data: {

                    }
                });

                nestedrequest2.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.log(response);

                    if (success) {
                        for (i = 0; i < data.length; i++) {
                            var rate = data[i].rate;
                            rate == null ? rate = '-' : rate = rate.toFixed(1);
                            $("#ratingGigs-" +  data[i].gigs_id).append(rate);
                        }
                    } else {
                        console.error(response);
                    }
                });

                nestedrequest2.fail(function(response) {
                    console.error(response);
                });
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestClass = $.ajax({
            url: 'http://103.129.223.136:2099/public/gigs/1',
            type: 'get',
            data: {

            }
        });

        requestClass.done(function(response) {
            var success = response.success;
            var message = response.message;
            classData = response.data;

            console.log("class",response);

            if (success) {
                classData.reverse();

                if (classData.length > 0) {
                    for (i = 0; i < classData.length; i++) {
                        var slidestyle = "";
                        var dotstyle = "";
                        if (i == 0) {
                            slidestyle = "display: block";
                            dotstyle = " active";
                        }

                        console.log("class check", classData.length);
                        $("#slide-item-container").append(`
                            <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + classData[i].id + `'" class="slide-item slide-fade" style="cursor: pointer; ` + slidestyle + `">
                                <img src="<?php echo base_url('assets/package/master/'); ?>` + classData[i].image + `" class="slide-image">
                                

                                <div class="slide-overlay"></div>
                                <div class="slide-mitra">
                                    <p class="paragraph-small mr-2">` + classData[i].mitra_name + `</p>
                                    <a href="<?php echo base_url('detail_terapis') ?>?id=` + classData[i].mitra_id + `"><img src="` + classData[i].mitra_photo + `" class="img-profile"></a>    
                                </div>
                                <div class="slide-title"><h3 class="mt-2">` + classData[i].name + `</h3></div>
                            </div>
                        `);

                        $("#slide-item-indicator").append(`<span class="slide-dot ` + dotstyle + `" onclick="currentSlide(` + i + `)"></span>`);
                    }
                    // $('#classTitle').text(classData[0].name);
                    // $('#classMitraName').text(classData[0].mitra_name);
                    // $('#classProfile').attr("src", classData[0].mitra_photo);
                    // $('#classImg').attr("src", '<?php echo base_url('assets/package/master/'); ?>' + classData[0].image);
                    // $('#classDesc').text(classData[0].desc.substring(0, 200));
                    // $('#classHref').attr('href', '<?php echo base_url('detail_layanan?gigs=') ?>' + classData[0].id);
                } else {
                    document.getElementById("classGigsContainer").style.display = "none";
                }
            }
        });

        requestClass.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });
</script>