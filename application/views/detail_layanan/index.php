<div style="padding-top: 100px"></div>

<div class="col-12 container row res-p-4" id="main-content" style="margin-bottom: 20px">
    <!-- ----- LEFT CONTENT ----- -->
    <div class="col-12 col-lg-8 p-0 pr-0 pr-lg-4">

        <!-- ----- TITLE ----- -->
        <h1 class="title-medium color-black" id="gigTitle"></h1>

        <!-- ----- TERAPIS ATTRIBUTE ----- -->
        <div class="col-12 row mt-4 align-items-center">
            <a id="userProfile" href="<?php echo base_url("detail_terapis") ?>" class="d-flex align-items-center">
                <img id="mitraPhotoTop" class="img-profile" src="<?php echo base_url('assets/img/img1.jpg') ?>">
                <h4 class="ml-2" id="mitraUsernameTop"></h4>
            </a>

            <!-- ----- RATING ----- -->
            <div class="ml-3 align-items-center mt-1" id="gigsRatingContainer">
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
                <i class='bx bxs-star color-primary' style="font-size: 20px; margin-left: -2px"></i>
            </div>
            <h4 class="ml-2 color-primary" id="gigsRating"></h4>
        </div>

        <img class="w-100 br-12 mt-4" id="gigsImage">


        <!-- ----- TENTANG LAYANAN ----- -->
        <h2 class="mt-4">Tentang Layanan Ini</h2>
        <p class="paragraph-medium mt-2" id="gigDesc"></p>

        <!-- ----- PERBANDINGAN PAKET ----- -->
        <h2 class="mt-4" id="perbandinganPaket">Perbandingan Paket</h2>
        <div class="col-12 row grid-2 m-0 p-0 mt-2" id="packageList">
        </div>

        <!-- ----- TERAPIS ATIBUTE ----- -->
        <a id="mitraProfileBottom" href="<?php echo base_url("detail_terapis") ?>" class="col-12 row card-transparent m-0 p-4" style="margin-top: 40px !important; margin-bottom: 40px !important">
            <img id="mitraPhotoBottom" class="img-profile" style="width: 80px; height: 80px" src="<?php echo base_url('assets/img/img1.jpg') ?>">

            <div class="d-flex flex-column justify-content-center ml-4">
                <h3 id="mitraUsernameBottom"></h3>
                <p class="paragraph-medium">Mitra / Terapis</p>
            </div>
        </a>

        <!-- ----- TANGGAPAN DAN ULASAN ----- -->
        <div id="ratingContainer" style="display: none">
            <h2>Tanggapan & Ulasan</h2>
            <div class="col-12 p-0 mt-3 mb-4" id="ratingList">
            </div>
        </div>
    </div>

    <!-- ----- RIGHT CONTENT ----- -->
    <div class="col-12 col-lg-4 p-0 sidebar" id="sidebar">
        <div class="sidebar__inner" style="transition: top .2s">
            <div id="daftarWarning" class="alert alert-warning mt-4" style="display: none">
                <strong>Perhatian!</strong> Layanan ini tidak memiliki paket yang tersedia!
            </div>
            <div id="daftarPaket" class="bg-primary br-12 p-4" style="position: relative; z-index: 1">

                <!-- ----- CONTENT ----- -->
                <h2 class="color-white">Daftar Paket</h2>
                <div class="mt-4 pt-2">
                    <form method="get" action="<?php echo base_url('checkout') ?>">
                        <input type="hidden" id="gigsId" name="gigs">

                        <div id="packageRadio">

                        </div>

                        <a id="btnBandingkan" href="#perbandinganPaket">
                            <p class="paragraph-small color-white text-right mt-4 mb-4">Bandingkan Paket</p>
                        </a>

                        <button id="btnSubmitLayanan" type="submit" class="btn btn-white w-100 pl-4 pr-4 text-left mt-4">
                            <div class="d-flex justify-content-between align-items-center">
                                <p class="paragraph-medium color-primary"><strong>Lanjutkan</strong></p>
                                <i class='bx bxs-chevrons-right color-primary' style="font-size: 20px;"></i>
                            </div>
                        </button>
                    </form>
                </div>

                <!-- ----- BACKGROUND ----- -->
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="68" preserveAspectRatio="none" viewBox="0 0 388 65.368" style="position: absolute; top: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_11" data-name="Path 11" d="M0,64.867s55.623,1.127,128.188,0,86.381-15.012,171.6-15.153S388,53.788,388,53.788V0H0Z" fill="#fff" opacity="0.2" />
                </svg>

                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="136" preserveAspectRatio="none" viewBox="0 0 388 135.976" style="position: absolute; bottom: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_3" data-name="Path 3" d="M388,123.175c-3.136,1.474-33.1,14.881-105.663,12.523S179.521,104.966,94.3,104.671,0,112.521,0,112.521V0H388Z" transform="translate(388 135.976) rotate(180)" fill="#fff" opacity="0.2" />
                </svg>

                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="136" preserveAspectRatio="none" viewBox="0 0 388 135.976" style="position: absolute; bottom: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_10" data-name="Path 10" d="M0,123.175c3.136,1.474,33.1,14.881,105.663,12.523S208.479,104.966,293.7,104.671s94.3,7.85,94.3,7.85V0H0Z" transform="translate(388 135.976) rotate(180)" fill="#fff" opacity="0.2" />
                </svg>
            </div>

            <h3 class="color-primary mt-4">Bagikan layanan</h3>
            <div class="d-flex pt-4 align-items-center">
                <a onclick="CopytoClipboard()" title="copy to clipboard" class="card-transparent m-0 p-0 mr-2 d-flex align-items-center justify-content-center" style="height: 28px; cursor: pointer; border-radius: 4px; padding: 0px 8px !important"><i class='bx bx-copy-alt bx-xs'></i><p style="font-size: 12px; margin: 0; margin-left: 8px">Copy URL</p></a>
                
                <div id="facebookshare" class="fb-share-button" 
                    data-href="https://www.kanalterapi.com" 
                    data-layout="button"
                    data-size="large">
                </div>

                <div class="mr-2"></div>

                <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false" data-size="large">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                <a id="whatsappShare" href="" data-action="share/whatsapp/share" class="card-transparent m-0 p-0 ml-2 d-flex align-items-center justify-content-center" style="height: 28px; cursor: pointer; border-radius: 4px; padding: 0px 8px !important; background-color: #4BC75A"><i class='bx bxl-whatsapp bx-xs color-white'></i><p class="color-white" style="font-size: 12px; margin: 0; margin-left: 8px">Whatsapp</p></a>
            </div>
        </div>
    </div>
</div>

<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript">
    var stickySidebar;

    function myFunction(x) {
        if (x.matches) {
            if (stickySidebar) {
                stickySidebar.destroy();
            }
        } else {
            stickySidebar = new StickySidebar('#sidebar', {
                innerWrapperSelector: '.sidebar__inner',
                containerSelector: false,
                topSpacing: 40,
            });
        }
    }

    var x = window.matchMedia("(max-width:992px)");
    myFunction(x);
    x.addListener(myFunction);
</script>


<script>
    $('#whatsappShare').attr('href', 'whatsapp://send?text=' + getLink())
    $('#facebookshare').attr('data-href', getLink())

    function getLink() {
        return window.location.href;
    }

    function CopytoClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(window.location.href).select();
        document.execCommand("copy");
        $temp.remove();
        alert("Success Copy to Clipboard");
    }
</script>

<script>
    var request;
    var requestGigs;
    var requestRating;
    var requestRatingAverage;
    var gigsPackage;
    var gigsData;
    var gigsRating;
    var gigsRatingAverage;

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request || requestGigs || requestRating) {
            request.abort();
            requestGigs.abort();
            requestRating.abort();
        }

        setTimeout(function() {
            if (stickySidebar) {
                stickySidebar.updateSticky();
            }
        }, 400);

        var id = getUrlParameter('gigs');

        document.getElementById('gigsId').value = id;

        request = $.ajax({
            url: 'http://103.129.223.136:2099/gigs-package/' + id,
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsPackage = response.data;

            console.log(response);

            if (success) {
                if (gigsPackage.length == 0) {
                    $("#daftarPaket").css('display', 'none');
                    $("#daftarWarning").css('display', 'block');
                }
                for (i = 0; i < gigsPackage.length; i++) {
                    var value = gigsPackage[i];
                    if (i == 0) {
                        $("#packageRadio").append(`
                            <label class="paket-radio-container">
                                <input type="radio" name="package" value="` + value.id + `" checked>
                                <div class="content">
                                    <h4>` + value.name + `</h4>
                                    <p>Rp ` + numberWithCommas(value.price) + `</p>
                                    <i class='bx bxs-check-circle' style="font-size: 20px;"></i>
                                </div>
                            </label>
                        `);
                    } else {
                        $("#packageRadio").append(`
                            <label class="paket-radio-container">
                                <input type="radio" name="package" value="` + value.id + `">
                                <div class="content">
                                    <h4>` + value.name + `</h4>
                                    <p>Rp ` + numberWithCommas(value.price) + `</p>
                                    <i class='bx bxs-check-circle' style="font-size: 20px;"></i>
                                </div>
                            </label>
                        `);
                    }


                    $("#packageList").append(`
                        <div class="card-transparent">
                            <h3>` + value.name + `</h3>

                            <p class="paragraph-small mt-3">` + value.desc + `</p>

                            <p class="paragraph-medium mt-4 mb-2">Harga <strong>Rp ` + numberWithCommas(value.price) + `</strong></p>

                            <a href="<?php echo base_url('checkout'); ?>?gigs=` + id + `&package=` + value.id + `">
                                <div class="btn-primary-transparent text-center">
                                    <h4 class="color-primary">Pilih</h4>
                                </div>
                            </a>
                        </div>
                    `);

                    if (stickySidebar) {
                        stickySidebar.updateSticky();
                    }
                }
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestGigs = $.ajax({
            url: 'http://103.129.223.136:2099/public/gigs/0',
            type: 'get',
            data: {

            }
        });

        requestGigs.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsData = response.data;

            console.log(response);

            if (success) {
                var gigByID = gigsData.filter(function(val) {
                    return val.id == id
                });

                $("#gigTitle").append(gigByID[0].name);
                $("#gigDesc").append(gigByID[0].desc);
                $("#gigsImage").attr("src", '<?php echo base_url('assets/package/master/'); ?>' + gigByID[0].image);
                $("#mitraUsernameTop").append(gigByID[0].mitra_name);
                $("#mitraUsernameBottom").append(gigByID[0].mitra_name);
                $("#mitraPhotoTop").attr("src", gigByID[0].mitra_photo);
                $("#mitraPhotoBottom").attr("src", gigByID[0].mitra_photo);
                $("#userProfile").attr("href", "<?php echo base_url("detail_terapis?id=") ?>" + gigByID[0].mitra_id);
                $("#mitraProfileBottom").attr("href", "<?php echo base_url("detail_terapis?id=") ?>" + gigByID[0].mitra_id);

                $("meta[property='og\\:url']").attr("content", window.location.href);
                $("meta[property='og\\:title']").attr("content", 'Kanalterapi - ' + gigByID[0].name);
                $("meta[property='og\\:description']").attr("content", gigByID[0].desc);
                $("meta[property='og\\:image']").attr("content", '<?php echo base_url('assets/package/master/'); ?>' + gigByID[0].image);

                if (stickySidebar) {
                    stickySidebar.updateSticky();
                }
            } else {
                console.error(response);
            }
        });

        requestGigs.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestRating = $.ajax({
            url: 'http://103.129.223.136:2099/gigs-rate/' + id,
            type: 'get',
            data: {

            }
        });

        requestRating.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsRating = response.data;

            console.log(response);

            if (success) {
                if (gigsRating.length > 0) {
                    document.getElementById('ratingContainer').style.display = 'block';
                }
                for (i = 0; i < gigsRating.length; i++) {
                    var value = gigsRating[i];
                    $("#ratingList").append(`
                        <div class="d-flex mt-4">
                            <a href="#" class="img-profile ml-2 d-flex justify-content-center align-items-center" style="background-color: rgba(57, 57, 57, 0.08)">
                                <h2>` + value.name.substring(0, 1) + `</h2>
                            </a>
                            <div class="ml-3">
                                <div class="d-flex">
                                    <p class="paragraph-medium" style="font-weight: 500">` + value.name + `</p>
                                    <i class='bx bxs-star color-primary ml-2' style="font-size: 20px;"></i>
                                    <h4 class="ml-1 color-primary">` + value.rating + `</h4>
                                </div>
                                <p class="paragraph-small mt-2">` + value.comment + `</p>
                            </div>
                        </div>
                    `);
                }

                if (stickySidebar) {
                    stickySidebar.updateSticky();
                }
            } else {
                console.error(response);
            }
        });

        requestRating.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestRatingAverage = $.ajax({
            url: 'http://103.129.223.136:2099/gigs-rate-average/' + id,
            type: 'get',
            data: {

            }
        });

        requestRatingAverage.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsRatingAverage = response.data;

            console.log(response);

            if (success) {
                var rate = gigsRatingAverage[0].rate;
                if (rate == null) {
                    document.getElementById('gigsRatingContainer').style.display = 'none';
                } else {
                    $("#gigsRating").append(rate.toFixed(1));
                    document.getElementById('gigsRatingContainer').style.display = 'block';
                }
            } else {
                console.error(response);
            }
        });

        requestRatingAverage.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });
</script>