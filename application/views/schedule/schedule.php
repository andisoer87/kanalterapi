<div style="padding-top: 100px"></div>


<div class="col-12 container row res-p-4" id="main-content" style="margin-bottom: 20px; min-height: calc(100% - 340px);">
    <!-- ----- LEFT CONTENT ----- -->
    <div class="col-lg-8 p-0 pr-0 pr-lg-4">

        <!-- ----- TITLE ----- -->
        <h1 class="title-medium color-black">Layanan yang dipilih</h1>

        <div class="overflow-auto">
            <table class="table col-12 mt-4">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col" colspan="2">Nama Layanan</th>
                        <th scope="col">Paket</th>
                        <th scope="col">Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td><img id="gigsImage" class="mr-3" style="width: 100px; border-radius: 8px"></td>
                        <td id="gigsName"></td>
                        <td id="gigsPackageName"></td>
                        <td class="gigsPackagePrice">Rp </td>
                    </tr>
                    <tr style="background-color: #f3f3f3">
                        <th scope="row" colspan="4" class="text-center">Total</th>
                        <th class="gigsPackagePrice">Rp </th>
                    </tr>
                </tbody>
            </table>
        </div>

        <div id="LoginError" class="alert alert-warning alert-dismissible mt-4">
            <strong>Perhatian!</strong> waktu pelaksaan akan di konfirmasi ulang oleh terapis!
        </div>
    </div>

    <!-- ----- RIGHT CONTENT ----- -->
    <div class="col-lg-4 p-0">
        <div>
            <div class="bg-primary br-12 p-4" style="position: relative; z-index: 1">

                <!-- ----- CONTENT ----- -->
                <h2 class="color-white">Pemilihan Tanggal</h2>
                <div class="mt-4 pt-2">
                    <form id="form">
                        <p class="paragraph-small color-white mb-2">Tanggal Pelaksanaan</p>
                        <input class="form-control border-0 p-4 color-primary" style="border-radius: 8px" id="date" name="date" type="text" value="<?php echo date('d M yy') ?>" autocomplete="off" />

                        <p class="paragraph-small color-white mt-3 mb-2">Waktu Pelaksanaan</p>
                        <input class="form-control border-0 p-4 color-primary" style="border-radius: 8px" id="time" name="date" type="text" value="<?php echo date('H : i') ?>" autocomplete="off" />

                        <h3 class="paragraph-small color-white text-left mt-4 pt-2 mb-2">Pembayaran akan dikirim ke email!</h3>

                        <button class="btn btn-white w-100 pl-4 pr-4 text-left">
                            <div class="d-flex justify-content-between align-items-center">
                                <p class="paragraph-medium color-primary"><strong>Atur Jadwal</strong></p>
                                <i class='bx bxs-chevrons-right color-primary' style="font-size: 20px;"></i>
                            </div>
                        </button>
                    </form>
                </div>

                <!-- ----- BACKGROUND ----- -->
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="68" preserveAspectRatio="none" viewBox="0 0 388 65.368" style="position: absolute; top: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_11" data-name="Path 11" d="M0,64.867s55.623,1.127,128.188,0,86.381-15.012,171.6-15.153S388,53.788,388,53.788V0H0Z" fill="#fff" opacity="0.2" />
                </svg>

                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="136" preserveAspectRatio="none" viewBox="0 0 388 135.976" style="position: absolute; bottom: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_3" data-name="Path 3" d="M388,123.175c-3.136,1.474-33.1,14.881-105.663,12.523S179.521,104.966,94.3,104.671,0,112.521,0,112.521V0H388Z" transform="translate(388 135.976) rotate(180)" fill="#fff" opacity="0.2" />
                </svg>

                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="136" preserveAspectRatio="none" viewBox="0 0 388 135.976" style="position: absolute; bottom: 0; left: 0; right: 0; z-index: -1">
                    <path id="Path_10" data-name="Path 10" d="M0,123.175c3.136,1.474,33.1,14.881,105.663,12.523S208.479,104.966,293.7,104.671s94.3,7.85,94.3,7.85V0H0Z" transform="translate(388 135.976) rotate(180)" fill="#fff" opacity="0.2" />
                </svg>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="SuccessModal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Berhasil Memilih Tanggal</h5>
                </div>
                <div class="modal-body">
                    <p class="paragraph-small">Silahkan tunggu kabar lebih lanjut dari terapis! informasi lebih lanjut akan dikirimkan pada email!</p>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo base_url('order') ?>" class="btn btn-primary color-white">Mengerti</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Gagal Membuat Pesanan!</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="paragraph-small">Terdapat kesalahan pada saat membuat pesanan: </p>
                    <div class="alert alert-danger mt-4" role="alert">
                        <p class="m-0 paragraph-small">Error Message : <span id="ErrorModalMessage"></span></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <a type="button" class="btn btn-danger color-white" data-dismiss="modal">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        var date_input = $('#date'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";

        var options = {
            format: 'dd M yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
            orientation: "top auto",
        };
        date_input.datepicker(options);
    })
</script>

<script type='text/javascript'>
    $('#time').timepicki({
        show_meridian: false,
        min_hour_value: 0,
        max_hour_value: 23,
        step_size_minutes: 1,
        overflow_minutes: true,
        increase_direction: 'up',
        disable_keyboard_mobile: true
    });
</script>

<script>
    var request;
    var requestGigs;
    var gigsPackage;
    var gigsData;
    var currentPackages;
    var id;
    var packages;

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request) {
            request.abort();
        }

        id = getUrlParameter('gigs');
        packages = getUrlParameter('package');

        request = $.ajax({
            url: 'http://103.129.223.136:2099/gigs-package/' + id,
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsPackage = response.data;

            console.log(response);

            if (success) {
                var gigByID = gigsPackage.filter(function(val) {
                    return val.id == packages
                });

                currentPackages = gigByID[0];

                $('#gigsPackageName').append(numberWithCommas(currentPackages.name));
                $('.gigsPackagePrice').append(numberWithCommas(currentPackages.price));

            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });

        requestGigs = $.ajax({
            url: 'http://103.129.223.136:2099/public/gigs',
            type: 'get',
            data: {

            }
        });

        requestGigs.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsData = response.data;

            console.log(response);

            if (success) {
                var gigByID = gigsData.filter(function(val) {
                    return val.id == id
                });

                $("#gigsName").append(gigByID[0].name);
                $("#gigsImage").attr("src", '<?php echo base_url('assets/package/master/'); ?>' + gigByID[0].image);
            } else {
                console.error(response);
            }
        });

        requestGigs.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });

    $("#form").submit(function(event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }

        transaction_id = getUrlParameter('transaction');

        moment().format();

        var date = document.getElementById('date').value;
        var time = document.getElementById('time').value;

        var value = moment(date + " / " + time, "DD MMM YYYY / HH : mm");
        var date_proposed = moment(value).format("YYYY-MM-DD hh:mm:ss");

        if ('<?php echo $this->session->userdata('token'); ?>' != '' && transaction_id != null && date_proposed != null) {
            request = $.ajax({
                url: 'http://103.129.223.136:2099/schedule/customer',
                type: 'post',
                headers: {
                    'access-token': '<?php echo $this->session->userdata('token'); ?>'
                },
                data: {
                    transaction_id: transaction_id,
                    date_proposed: date_proposed
                }
            });

            request.done(function(response) {
                var success = response.success;
                var message = response.message;
                gigsPackage = response.data;

                console.log(response);

                if (success) {
                    $('#SuccessModal').modal('show');
                } else {
                    console.error(response);
                    $('#ErrorModal').modal('show');
                    $('#ErrorModalMessage').html(message);
                }
            });

            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;

                console.error(response);
                $('#ErrorModal').modal('show');
                $('#ErrorModalMessage').html(message);
            });
        }

    });
</script>