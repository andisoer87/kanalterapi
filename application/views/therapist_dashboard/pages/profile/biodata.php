
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Biodata Anda</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Profile</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-info card-outline">
              <div class="card-body box-profile">
                <div class="text-center" id="user-avatar">
                  
                </div>
                <br>
                <button class="btn btn-success btn-block btn-sm" data-toggle="modal" data-target="#edit-photo">Ganti foto</button>
                <hr>
                <h3 class="profile-username text-center" id="user-name"> </h3>

                <p class="text-muted text-center" id="user-email"> </p>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Tentang saya</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fas fa-phone mr-1"></i> Telepon</strong>

                <p class="text-muted" id="user-phone">
                </p>

                <hr>

                <strong><i class="fas fa-map-marker-alt mr-1"></i> Lokasi</strong>

                <p class="text-muted"><span id="user-city"></span>, <span id="user-province"></span></p>

                <hr>

                <input type="hidden" class="name-safe" name="name-safe" id="name-safe"/>
                <input type="hidden" class="telp-safe" name="telp-safe" id="telp-safe"/>
                <input type="hidden" class="cv-safe" name="cv-safe" id="cv-safe"/>
                <input type="hidden" class="porto-safe" name="porto-safe" id="porto-safe"/>
                <textarea class="desc-safe" name="desc-safe" id="desc-safe" style="display:none;"></textarea>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#timeline" data-toggle="tab">CV & Portofolio</a></li>
                  <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <div class="timeline timeline-inverse">
                      <!-- timeline time label -->
                      <div class="time-label">
                        <span class="bg-teal">
                          Curriculum Vitae
                        </span>
                      </div>
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <div class="timeline-footer" id="show-cv">

                          </div>
                        </div>
                      </div>
                      <!-- END timeline item -->
                      <!-- timeline time label -->
                      <div class="time-label">
                        <span class="bg-teal">
                          Portofolio
                        </span>
                      </div>
                      <div>
                        <i class="fas fa-user bg-info"></i>

                        <div class="timeline-item">
                          <div class="timeline-footer" id="show-porto">
                            
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.tab-pane -->

                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" id="show-form">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName" placeholder="Nama">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">No Telepon</label>
                        <div class="col-sm-10">
                          <input type="telepon" class="form-control" id="inputTelepon" placeholder="Telepon">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Deskrisi tentang anda"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button id="btn_save" class="btn btn-danger">Simpan</button>
                          <!-- <button id="btn_cancle" class="btn btn-grey">Kembali</button> -->
                        </div>
                      </div><!-- <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nama</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="readName" placeholder="Nama" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">No Telepon</label>
                        <div class="col-sm-10">
                          <input type="telepon" class="form-control" id="readTelepon" placeholder="Telepon" readonly>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Deskripsi</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="readExperience" placeholder="Deskrisi tentang anda" readonly></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button id="btn_edit" class="btn btn-success">Edit</button>
                        </div>
                      </div> -->
                    </form>
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
            
            <!-- /.modal -->
            <div class="modal fade" id="edit-photo">
              <div class="modal-dialog modal-md">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit foto anda</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form action="<?php echo base_url('therapist_dashboard_control/profile_upload'); ?>" method="post" enctype="multipart/form-data">
                  <div class="modal-body">
                    <div class="card-body">
                      <input type="hidden" name="3name-safe" class="name-safe">
                      <input type="hidden" name="3telp-safe" class="telp-safe">
                      <input type="hidden" name="3cv-safe" class="cv-safe">
                      <input type="hidden" name="3porto-safe" class="porto-safe">
                      <textarea style="display: none;" name="3desc-safe" class="desc-safe"> </textarea>
                      <div class="form-group">
                        <label for="input-file">Gambar</label><br>
                        <input type="file" name="gambar">
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <input type="submit" class="btn bg-cyan" value="Simpan">
                  </div>
                  </form>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
    </section>
    <!-- /.content -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

