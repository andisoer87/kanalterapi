<!DOCTYPE html>
<html>
<head>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script>
  $(document).ready(function(){

    send_procces();

    function send_procces(){
        var name = '<?php echo $data_name; ?>';
        var telp = '<?php echo $data_phone; ?>';
        var cv = '<?php echo $data_cv; ?>';
        var port = '<?php echo $data_pt; ?>';
        var desc = '<?php echo $data_desc; ?>';

        request = $.ajax({
                        url: 'http://103.129.223.136:2099/me/mitra',
                        type: 'put',
                        headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                        data: {
                            name: name,
                            handphone: telp,
                            curriculum_vitae: cv,
                            category: '0',
                            portofolio: port,
                            description: desc
                        }
                    });

        request.done(function(response) {
            window.location.href = "<?php echo base_url('Therapist_dashboard_control/my_profile') ?>";
        });
        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;
        });
    }
  });
</script>
</body>
</html>