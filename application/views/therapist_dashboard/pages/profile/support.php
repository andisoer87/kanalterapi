<script>
  $(document).ready(function(){

    show_me();
    
    function show_me(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/me/mitra',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var avatar = '';
              var name = '';
              var email = '';
              var phone = '';
              var city = '';
              var province = '';
              var cv = '';
              var porto = '';
              var show_cv = '';
              var show_porto = '';
              var desc = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  avatar += '<img class="profile-user-img img-fluid img-circle" src="'+obj.data[i].photo+'" alt="User profile picture">';
                  name += obj.data[i].name;
                  email += obj.data[i].email;
                  phone += obj.data[i].handphone;
                  city += obj.data[i].kota;
                  province += obj.data[i].provinsi;
                  cv += obj.data[i].curriculum_vitae;
                  porto += obj.data[i].portofolio;
                  desc += obj.data[i].description;

                  if(obj.data[i].curriculum_vitae == "notset"){
                    show_cv += "<a onclick=\"show_cv_form_upload('"+name+"','"+phone+"','"+porto+"','"+desc+"')\" class=\"btn btn-success\">Upload</a>";
                  }else{
                    show_cv += "<a class=\"btn btn-primary btn-md\" href=\"<?php echo base_url(); ?>assets/mitra/cv/"+cv+"\" download=\""+name+" | CV\">Download</a>"+
                               " <a onclick=\"show_after_cv_form_upload('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-success btn-md\">Ganti File</a>";
                  }
                  if(obj.data[i].portofolio == "notset"){
                    show_porto += "<a onclick=\"show_porto_form_upload()\" class=\"btn btn-success\">Upload</a>";
                  }else{
                    show_porto += "<a class=\"btn btn-primary btn-md\" href=\"<?php echo base_url(); ?>assets/mitra/pt/"+porto+"\" download=\""+name+" | PORTOFOLIO\">Download</a>"+
                                  " <a onclick=\"show_after_porto_form_upload('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-success btn-md\">Ganti File</a>";
                  }
              }
              $('#user-avatar').html(avatar);
              $('#user-name').html(name);
              $('#user-email').html(email);
              $('#user-phone').html(phone);
              $('#user-city').html(city);
              $('#user-province').html(province);
              $('#show-cv').html(show_cv);
              $('#show-porto').html(show_porto);
              $('.name-safe').val(name);
              $('.telp-safe').val(phone);
              $('.cv-safe').val(cv);
              $('.porto-safe').val(porto);
              $('.desc-safe').html(desc);
              $('#inputName').val(name);
              $('#inputTelepon').val(phone);
              $('#inputExperience').html(desc);
              $('#readName').val(name);
              $('#readTelepon').val(phone);
              $('#readExperience').html(desc);
          }
      });
    }

    $('#btn_save').on('click',function(){
      
      name = $('#inputName').val();
      telp = $('#inputTelepon').val();
      cv = $('#cv-safe').val();
      port = $('#porto-safe').val();
      desc = $('#inputExperience').val();

      request = $.ajax({
                    url: 'http://103.129.223.136:2099/me/mitra',
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        name: name,
                        handphone: telp,
                        curriculum_vitae: cv,
                        category: '0',
                        portofolio: port,
                        description: desc
                    }
                });

      request.done(function(response) {
          window.location.href = "<?php echo base_url('Therapist_dashboard_control/my_profile') ?>";
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function show_cv_form_upload(name,phone,porto,desc){
    html = "<form method=\"post\" action=\"cv_upload\" enctype=\"multipart/form-data\">"+
            "<input type=\"file\" name=\"cv\" />"+
            "<input type=\"hidden\" value=\""+name+"\" class=\"name-safe\" name=\"1name-safe\" id=\"1name-safe\"/>"+
            "<input type=\"hidden\" value=\""+phone+"\" class=\"telp-safe\" name=\"1telp-safe\" id=\"1telp-safe\"/>"+
            "<input type=\"hidden\" value=\""+porto+"\" class=\"porto-safe\" name=\"1porto-safe\" id=\"1porto-safe\"/>"+
            "<textarea class=\"desc-safe\" name=\"1desc-safe\" id=\"1desc-safe\" style=\"display:none;\">"+desc+"</textarea>"+
            "<input type=\"submit\" class=\"btn btn-danger text-white\" value=\"Upload\"/>"+
            " <button onclick=\"back_cv('"+name+"','"+phone+"','"+porto+"','"+desc+"')\" class=\"btn btn-default\">Close</button>"+
           "</form>";
    $('#show-cv').html(html);
  }

  function show_after_cv_form_upload(name,phone,cv,porto,desc){
    html = "<form method=\"post\" action=\"cv_upload\" enctype=\"multipart/form-data\">"+
            "<input type=\"file\" name=\"cv\" />"+
            "<input type=\"hidden\" value=\""+name+"\" class=\"name-safe\" name=\"1name-safe\" id=\"1name-safe\"/>"+
            "<input type=\"hidden\" value=\""+phone+"\" class=\"telp-safe\" name=\"1telp-safe\" id=\"1telp-safe\"/>"+
            "<input type=\"hidden\" value=\""+porto+"\" class=\"porto-safe\" name=\"1porto-safe\" id=\"1porto-safe\"/>"+
            "<textarea class=\"desc-safe\" name=\"1desc-safe\" id=\"1desc-safe\" style=\"display:none;\">"+desc+"</textarea>"+
            "<input type=\"submit\" class=\"btn btn-danger text-white\" value=\"Upload\"/>"+
            " <button onclick=\"back_after_cv('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-default\">Close</button>"+
          "</form>";
    $('#show-cv').html(html);
  }

  function show_porto_form_upload(name,phone,cv,desc){
    html = "<form method=\"post\" action=\"pt_upload\" enctype=\"multipart/form-data\">"+
            "<input type=\"file\" name=\"pt\" />"+
            "<input type=\"hidden\" value=\""+name+"\" class=\"name-safe\" name=\"2name-safe\" id=\"2name-safe\"/>"+
            "<input type=\"hidden\" value=\""+phone+"\" class=\"telp-safe\" name=\"2telp-safe\" id=\"2telp-safe\"/>"+
            "<input type=\"hidden\" value=\""+cv+"\" class=\"cv-safe\" name=\"2cv-safe\" id=\"2cv-safe\"/>"+
            "<textarea class=\"desc-safe\" name=\"2desc-safe\" id=\"2desc-safe\" style=\"display:none;\">"+desc+"</textarea>"+
            "<input type=\"submit\" class=\"btn btn-danger text-white\" value=\"Upload\"/>"+
            " <button onclick=\"back_porto('"+name+"','"+phone+"','"+cv+"','"+desc+"')\" class=\"btn btn-default\">Close</button>"+
           "</form>";
    $('#show-porto').html(html);
  }

  function show_after_porto_form_upload(name,phone,cv,porto,desc){
    html = "<form method=\"post\" action=\"pt_upload\" enctype=\"multipart/form-data\">"+
            "<input type=\"file\" name=\"pt\" />"+
            "<input type=\"hidden\" value=\""+name+"\" class=\"name-safe\" name=\"2name-safe\" id=\"2name-safe\"/>"+
            "<input type=\"hidden\" value=\""+phone+"\" class=\"telp-safe\" name=\"2telp-safe\" id=\"2telp-safe\"/>"+
            "<input type=\"hidden\" value=\""+cv+"\" class=\"cv-safe\" name=\"2cv-safe\" id=\"2cv-safe\"/>"+
            "<textarea class=\"desc-safe\" name=\"2desc-safe\" id=\"2desc-safe\" style=\"display:none;\">"+desc+"</textarea>"+
            "<input type=\"submit\" class=\"btn btn-danger text-white\" value=\"Upload\"/>"+
            " <button onclick=\"back_after_porto('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-default\">Close</button>"+
          "</form>";
    $('#show-porto').html(html);
  }

  function back_cv(name,phone,porto,desc){
    html = "<a onclick=\"show_cv_form_upload('"+name+"','"+phone+"','"+porto+"','"+desc+"')\" class=\"btn btn-success\">Upload</a>";
    $('#show-cv').html(html);
  }

  function back_after_cv(name,phone,cv,porto,desc){
    html = "<a class=\"btn btn-primary btn-md\" href=\"<?php echo base_url(); ?>assets/mitra/cv/"+cv+"\" download=\""+name+" | CV\">Download</a>"+
           " <a onclick=\"show_after_cv_form_upload('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-success btn-md\">Ganti File</a>";
    $('#show-cv').html(html);
  }

  function back_porto(){
    html = "<a onclick=\"show_porto_form_upload()\" class=\"btn btn-success\">Upload</a>";
    $('#show-porto').html(html);
  }

  function back_after_porto(name,phone,cv,porto,desc){
    html = "<a class=\"btn btn-primary btn-md\" href=\"<?php echo base_url(); ?>assets/mitra/pt/"+porto+"\" download=\""+name+" | PT\">Download</a>"+
          " <a onclick=\"show_after_porto_form_upload('"+name+"','"+phone+"','"+cv+"','"+porto+"','"+desc+"')\" class=\"btn btn-success btn-md\">Ganti File</a>";
    $('#show-porto').html(html);
  }
</script>