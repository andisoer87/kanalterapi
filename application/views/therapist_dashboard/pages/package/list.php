
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Data paket yang tersedia</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Paket</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <button type="button" class="btn bg-cyan" data-toggle="modal" data-target="#add-package"><i class="fas fa-plus"></i> Tambah paket privat</button>
              <button type="button" class="btn bg-olive" data-toggle="modal" data-target="#add-1package"><i class="fas fa-plus"></i> Tambah kelas</button>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- /.modal -->
      <div class="modal fade" id="add-package">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah data paket</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form id="submit" action="<?php echo base_url('therapist_package_control/upload_file'); ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="card-body">
                <div class="form-group">
                  <label for="input-name">Nama</label>
                  <input type="text" class="form-control" id="input-name" name="name" placeholder="Nama paket">
                </div>
                <div class="form-group">
                  <label for="input-image">Gambar</label><br>
                  <input type="file" name="image" id="input-image">
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" id="input-desc" name="desc" placeholder="Isi deskripsi ..."></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <input type="submit" id="btn_sav" class="btn bg-cyan" value="Simpan">
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- /.modal -->
      <div class="modal fade" id="add-1package">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah kelas</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form id="submit" action="<?php echo base_url('therapist_package_control/upload_class'); ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="card-body">
                <div class="form-group">
                  <label for="input-name">Nama</label>
                  <input type="text" class="form-control" id="input-name" name="name" placeholder="Nama paket">
                </div>
                <div class="form-group">
                  <label for="input-name">Judul kelas</label>
                  <input type="text" class="form-control" id="input-class" name="class" placeholder="Judul kelas paket">
                </div>
                <div class="form-group">
                  <label for="input-name">Kapasitas Kelas</label>
                  <input type="number" class="form-control" id="input-capacity" name="capacity">
                </div>
                <div class="form-group">
                  <label for="input-name">Media yang digunakan</label>
                  <input type="text" class="form-control" id="input-media" name="media" placeholder="Media yang akan dipakai">
                </div>
                <div class="form-group">
                  <label for="input-image">Gambar</label><br>
                  <input type="file" name="image" id="input-image">
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" id="input-desc" name="desc" placeholder="Isi deskripsi ..."></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <input type="submit" id="btn_sav" class="btn bg-cyan" value="Simpan">
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>

    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Jadwal pelaksanaan terapi</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
            <table class="table" id="table-package">
                <thead>
                  <tr>
                    <th style="width: 1%">
                        #
                    </th>
                  </tr>
                </thead>
                <tbody id="show-data">
                  
                </tbody>
            </table>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.card-body -->
      </div>
            
      <!-- /.modal -->
      <div class="modal fade" id="detail-package">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Detail data paket</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card-body">
                <input type="hidden" name="id-detail" id="id-detail">
                <div class="text-center"><h4 id="title-detail"> </h4></div>
                <hr>
                <div class="text-center" id="image-detail"> </div>
                <br>
                <div id="desc-detail"> </div>
              </div>
            </div>
            <div class="modal-footer">
              <button data-dismiss="modal" class="btn bg-light">Tutup</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
            
      <!-- /.modal -->
      <div class="modal fade" id="edit-package">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Edit data paket</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="<?php echo base_url('therapist_package_control/edit_file'); ?>" method="post" enctype="multipart/form-data">
            <div class="modal-body">
              <div class="card-body">
                <div class="form-group">
                  <label for="input-name">Nama</label>
                  <input type="hidden" id="edit-id" name="edit-id">
                  <input type="text" class="form-control" id="edit-name" name="name" placeholder="Nama paket">
                </div>
                <div class="form-group">
                  <label for="input-file">Gambar</label><br>
                  <input type="file" name="image" id="edit-file">
                  <input type="hidden" name="image-old" id="file-old">
                </div>
                <div class="form-group">
                  <label>Deskripsi</label>
                  <textarea class="form-control" id="edit-desc" name="desc" placeholder="Isi deskripsi ..."></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <input type="submit" class="btn bg-cyan" value="Simpan">
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
      <!-- /.modal -->
      <div class="modal fade" id="delete-package">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-danger">
              <h4 class="modal-title">Delete data paket</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body bg-danger">
              <div class="card-body">
                Apakah anda yakin mau menghapus data paket <span id="delete-name"></span> secara permanen?
              </div>
            </div>
            <div class="modal-footer bg-danger">
              <button type="button" class="btn bg-white" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn bg-danger btn btn-outline-light" id="btn_delete">Iya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

