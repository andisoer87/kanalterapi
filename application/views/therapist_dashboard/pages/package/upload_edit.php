<!DOCTYPE html>
<html>
<head>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
<script>
  $(document).ready(function(){

    send_procces();

    function send_procces(){
        var id  = '<?php echo $data_id; ?>';
        var name  = '<?php echo $data_name; ?>';
        var image = '<?php echo $data_image; ?>';
        var desc  = '<?php echo $data_desc; ?>';

        request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs/'+id,
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        name: name,
                        image: image,
                        desc: desc
                    }
                });
        request.done(function(response) {
            window.location.href = "<?php echo base_url('Therapist_package_control') ?>";
        });
        request.fail(function(response) {
            alert('nah');
        });
    }
  });
</script>
</body>
</html>