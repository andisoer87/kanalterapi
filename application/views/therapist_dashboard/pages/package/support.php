<script>
  $(document).ready(function(){

    show_product();
    
    function show_product(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/gigs',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;
                  var btn_theme = '';
                  var btn_text = '';

                  if(obj.data[i].giggs_type == "2"){
                    btn_theme += 'teal';
                    btn_text += 'privat';
                  }else if(obj.data[i].giggs_type == "1"){
                    btn_theme += 'olive';
                    btn_text += 'umum';
                  }
                  
                  html += '<tr><th><div class="alert text-white alert-dismissible" style="margin: 2%;background-image: url(<?php echo base_url(); ?>/assets/img/bg-list.JPG);background-repeat: no-repeat;background-size: cover;border: 4px solid #008B8B;border-radius:20px;">'+
                            '<h5 style="padding-bottom:10px;"><i class="icon fas fa-box"></i> '+obj.data[i].name+
                            '<button title="Hapus" onclick="delete_package(\''+obj.data[i].id+'\',\''+obj.data[i].name+'\')" class="btn btn-default bg-transparent text-white float-right"><i class="fas fa-trash"></i></button>'+
                            '<span class="float-right text-transparent">&nbsp;</span><button title="Edit" onclick="update_package(\''+obj.data[i].id+'\',\''+obj.data[i].name+'\',\''+obj.data[i].image+'\',\''+obj.data[i].desc+'\')" class="btn btn-default bg-transparent text-white float-right"><i class="fas fa-pen"></i></button>'+
                            '<span class="float-right text-transparent">&nbsp;</span><button title="Detail" onclick="detail_package(\''+obj.data[i].id+'\',\''+obj.data[i].name+'\',\''+obj.data[i].image+'\',\''+obj.data[i].desc+'\')" class="btn btn-default bg-transparent text-white float-right"><i class="fas fa-clipboard"></i></button> </h5>'+
                            '<h6><button class="btn bg-'+btn_theme+'" style="width:145px;border: 1px solid #008B8B;">Terapi '+btn_text+'</button> <a href="<?php echo base_url('index.php/therapist_package_control/sub_package?var1='); ?>'+obj.data[i].id+'&var2='+obj.data[i].name+'" class="btn btn-default bg-transparent text-white float-right" style="text-decoration: none;">Daftar paket <i class="fas fa-archive"></i></a></h6>'+
                          '</div></th></tr>';
              }
              $('#show-data').html(html);

              $("#table-package").DataTable({
                "responsive": true,
                "autoWidth": false
              });
          }
      });
    }
    function show_response(){
      
      var name  = $('#input-name').val();
      var image = $('#input-image').val();
      var desc  = $('#input-desc').val();

      request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs',
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        name: name,
                        image: 'terapi1.jpg',
                        desc: desc,
                    }
                });
      request.done(function(response) {
          $('#add-package').modal('hide');
          show_product();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    }

    $('#btn_delete').on('click',function(){
      
      var id  = $('#delete-id').val();

      request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs/'+id,
                    type: 'delete',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                });

      request.done(function(response) {
          $('#delete-package').modal('hide');
          show_product();
          alert('Data berhasil dihapus!');
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function detail_package(id,name,image,desc){
    var display_image = '<image src="<?php echo base_url(); ?>assets/package/master/'+image+'" height="250px">';

    $('#id-detail').val(id);
    $('#title-detail').html(name);
    $('#image-detail').html(display_image);
    $('#desc-detail').html(desc);

    $('#detail-package').modal('show');
  }

  function update_package(id,name,image,desc){
    $('#edit-id').val(id);
    $('#edit-name').val(name);
    $('#file-old').val(image);
    $('#edit-desc').val(desc);

    $('#edit-package').modal('show');
  }

  function delete_package(id,name){
    $('#delete-id').val(id);
    $('#delete-name').html(name);
    
    $('#delete-package').modal('show');
  }
</script>