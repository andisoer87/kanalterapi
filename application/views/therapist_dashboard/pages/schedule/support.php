<script>
  var nestedrequest;
  var requestSchedule;
  var orderDataSchedule;
  $(document).ready(function(){

    requestSchedule = $.ajax({
        url: 'http://103.129.223.136:2099/schedule/mitra',
        type: 'get',
        headers: {
            'access-token': '<?php echo $this->session->userdata('token'); ?>'
        },
        data: {
        }
    });

    requestSchedule.done(function(response) {
        var success = response.success;
        var message = response.message;
        orderDataSchedule = response.data;
        console.log(response);
        if (success) {
            if (orderDataSchedule.length == 0) {
                $("#orderPenjadwalanList").append(`
                    <tr>
                        <td scope="row" class="text-center" colspan="7">tidak ada data</td>
                    </tr>
                `);
            }
            orderDataSchedule = orderDataSchedule.filter(function (item, index) {
                var nextIndex = index + 1;
                return orderDataSchedule[nextIndex] == null || orderDataSchedule[index].transaction_id != orderDataSchedule[nextIndex].transaction_id
            });
            orderDataSchedule.reverse();
            for (i = 0; i < orderDataSchedule.length; i++) {
                var value = orderDataSchedule[i];
                var status;
                var statusColor = '';
                var statusType = 0;
                var status_mitra = '';
                var status_customer = '';
                var approve_button = '';
                var reschedule_button = '';

                if (value.status_customer == 1 && value.status_mitra == 1) {
                    status = 'Jadwal Telah Diatur';
                    statusColor = 'success';
                    statusType = 3;
                    status_mitra += '<span class="badge badge-success">Seetuju</span>';
                    status_customer += '<span class="badge badge-success">Setuju</span>';
                    approve_button =  '<a class="btn btn-default btn-md">'+
                                          'Approve <i class="bx bx-check-circle"> '+
                                          '</i>'+
                                      '</a> ';
                    reschedule_button = '<a class="btn btn-default btn-md">'+
                                            'Re-Schedule <i class="bx bx-calendar">'+
                                            '</i>'+
                                        '</a>';
                } else if (value.status_customer == 1 && value.status_mitra == 0) {
                    status = 'Menunggu Konfirmasi Terapis';
                    statusColor = 'light';
                    statusType = 2;
                    status_mitra += '<span class="badge badge-danger">Belum setuju</span>';
                    status_customer += '<span class="badge badge-success">Setuju</span>';
                    approve_button =  '<a onclick="approve(\''+value.schedule_id+'\',\''+value.transaction_id+'\')" class="btn btn-success text-white btn-md">'+
                                          'Approve <i class="bx bx-check-circle"> '+
                                          '</i>'+
                                      '</a> ';
                    reschedule_button = '<a onclick="reschedule(\''+value.transaction_id+'\')" class="btn btn-info text-white btn-md">'+
                                            'Re-Schedule <i class="bx bx-calendar">'+
                                            '</i>'+
                                        '</a>';
                } else if (value.status_customer == 0 && value.status_mitra == 1) {
                    status = 'Menunggu Konfirmasi Anda';
                    statusColor = 'warning';
                    statusType = 1;
                    status_mitra += '<span class="badge badge-success">Setuju</span>';
                    status_customer += '<span class="badge badge-danger">Belum setuju</span>';
                    approve_button =  '<a class="btn btn-default btn-md">'+
                                          'Approve <i class="bx bx-check-circle"> '+
                                          '</i>'+
                                      '</a> ';
                    reschedule_button = '<a onclick="reschedule(\''+value.transaction_id+'\')" class="btn btn-info text-white btn-md">'+
                                            'Re-Schedule <i class="bx bx-calendar">'+
                                            '</i>'+
                                        '</a>';
                } else {
                    status = 'tidak diketahui';
                    statusColor = 'danger';
                    statusType = 0;
                    status_mitra += '<span class="badge badge-danger">Belum setuju</span>';
                    status_customer += '<span class="badge badge-danger">Belum setuju</span>';
                    approve_button =  '<a onclick="approve(\''+value.schedule_id+'\',\''+value.transaction_id+'\')" class="btn btn-success text-white btn-md">'+
                                          'Approve <i class="bx bx-check-circle"> '+
                                          '</i>'+
                                      '</a> ';
                    reschedule_button = '<a onclick="reschedule(\''+value.transaction_id+'\')" class="btn btn-info btn-md">'+
                                            'Re-Schedule <i class="bx bx-calendar">'+
                                            '</i>'+
                                        '</a>';
                }
                
                var action = '';
                if (statusType == 3) {
                    action = `<a data-toggle="tooltip" data-placement="top" title="Selesaikan Pesanan" onclick="selesaikan('` + value.gigs_id + `', '` + value.transaction_id + `')"><i class='bx bxs-chevrons-right color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>`;
                } else if (statusType == 2) {
                    action = '-';
                } else if (statusType == 1) {
                    action = `<a data-toggle="tooltip" data-placement="top" title="Ubah Jadwal" target="_blank" href="<?php echo base_url('schedule') ?>?transaction=` + value.transaction_id + `&gigs=` + value.gigs_id + `&package=` + value.gigs_package_id + `"><i class='bx bxs-pencil color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>
                            <a data-toggle="tooltip" data-placement="top" title="Setujui Jadwal" onclick="approveschedule( '` + value.schedule_id + `', '` + value.transaction_id + `', '1' )"><i class='bx bxs-paper-plane color-white bg-primary' style="padding: 12px; border-radius: 8px"></i></a>`;
                } else {
                    action = '?';
                }
                $("#orderPenjadwalanList").append(`<tr>`+
                                                    `<th>`+
                                                    value.gigs_name+` (`+value.gigs_package_name+`)`+
                                                    `</th>`+
                                                    `<td id="name_`+value.schedule_id+`"> </td>`+
                                                    `<th>`+
                                                    value.date_proposed.substr(0,10)+
                                                    `</th>`+
                                                    `<td>`+
                                                    value.date_proposed.substr(11,5)+
                                                    `</td>`+
                                                    `<td>`+
                                                        status_mitra+
                                                    `</td>`+
                                                    `<td>`+
                                                        status_customer+
                                                    `</td>`+
                                                    `<td>`+
                                                        approve_button+``+
                                                        reschedule_button+
                                                    `</td>`+
                                                  `</tr>`);
                                                  
                    nestedrequest = $.ajax({
                        url: 'http://103.129.223.136:2099/gigs-package/' + value.user_id,
                        type: 'get',
                        data: {

                        }
                    });

                    nestedrequest.done(function(response) {
                        var success = response.success;
                        var message = response.message;
                        var data = response.data;

                        console.log(response);

                        if (success) {
                            $("#name_" + data.schedule_id).html(data.priceGigs);
                        } else {
                            console.error(response);
                        }
                    });

                    nestedrequest.fail(function(response) {
                        console.error(response);
                    });
            }
            $(function() {
                $('[data-toggle="tooltip"]').tooltip();
            })
            $("#table-schedule").DataTable();
        } else {
            console.error(response);
        }
    });
    requestSchedule.fail(function(response) {
        var success = response.success;
        var message = response.message;
        var data = response.data;
        console.error(response);
    });

    $('#btn_reschedule').on('click',function(){

      var id = $('#id-transaction').val();
      var date = $('#date').val();
      var time = $('#time').val();

      var value = moment(date + " / " + time, "DD MMM YYYY / HH : mm");
      var date_proposed = moment(value).format("YYYY-MM-DD hh:mm:ss");
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/schedule/mitra',
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        date_proposed: date_proposed
                    }
                });
      request.done(function(response) {
          $('#id').val('');
          $('#date').val('');
          $('#time').val('');
          $('#reschedule-modal').modal('hide');
          show_schedule();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
    
    $('#btn_approve').on('click',function(){

      var id = $('#1id-schedule').val();
      var id1 = $('#1id-transaction').val();
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/schedule/mitra',
                    type: 'put',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        schedule_id: id,
                        transaction_id: id1,
                        status: '1'
                    }
                });
      request.done(function(response) {
          $('#1id-schedule').val();
          $('#1id-transaction').val();
          $('#approve-modal').modal('hide');
          show_schedule();
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function approve(id,id1){
    document.getElementById("1id-schedule").value = id;
    document.getElementById("1id-transaction").value = id1;
    $("#approve-modal").modal('show');
  }

  function reschedule(id){
    document.getElementById("id-transaction").value = id;
    $("#reschedule-modal").modal('show');
  }
</script>