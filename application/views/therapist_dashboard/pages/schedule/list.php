
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Schedule</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Paket</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Jadwal pelaksanaan terapi</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-striped projects" id="table-schedule">
              <thead>
                  <tr>
                      <th style="width: 15%">
                          Nama terapi
                      </th>
                      <th>
                          Details
                      </th>
                      <th>
                          Tanggal
                      </th>
                      <th>
                          Jam
                      </th>
                      <th>
                          Persetujuan mitra
                      </th>
                      <th>
                          Persetujuan pelanggan
                      </th>
                      <th class="text-center">
                          
                      </th>
                  </tr>
              </thead>
              <tbody id="orderPenjadwalanList">
                
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- /.modal -->
      <div class="modal fade" id="reschedule-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Atur ulang jadwal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" id="id-transaction" name="id-transaction">
                </div>
                <div class="form-group">
                  <input class="form-control" id="date" name="date" type="date" autocomplete="off" />
                </div>
                <div class="form-group">
                  <input class="form-control" id="time" name="date" type="time" autocomplete="off" />
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a class="btn bg-cyan" id="btn_reschedule">Submit</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      <!-- /.modal -->
      <div class="modal fade" id="approve-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header bg-success">
              <h4 class="modal-title">Penyetujuan jadwal terapi</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body bg-success">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" class="form-control" id="1id-schedule" name="id-schedule">
                  <input type="hidden" class="form-control" id="1id-transaction" name="id-transaction">
                </div>
                Apakah anda telah setuju dengan jadwal ini?
              </div>
            </div>
            <div class="modal-footer justify-content-between bg-success">
              <button class="btn btn-default" data-dismiss="modal">Tidak</button>
              <a class="btn bg-cyan" id="btn_approve">Iya</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

