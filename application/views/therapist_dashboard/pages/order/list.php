

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Paket</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <!-- ./row -->
        <div class="row">
          <div class="col-12 col-sm-12">
            <div class="card card-cyan card-tabs">
              <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Baru</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-one-messages-tab" data-toggle="pill" href="#custom-tabs-one-messages" role="tab" aria-controls="custom-tabs-one-messages" aria-selected="false">Selesai</a>
                  </li>
                </ul>
              </div>
              <div class="card-body">
                <div class="tab-content" id="custom-tabs-one-tabContent">
                  <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <table class="table table-striped projects" id="table-order">
                        <thead>
                            <tr>
                              <th style="width: 1%">
                                  #
                              </th>
                              <th style="width: 20%">
                                  detail
                              </th>
                              <th style="width: 30%">
                                  Nama client
                              </th>
                              <th>
                                  Total
                              </th>
                              <th style="width: 8%" class="text-center">
                                  Status
                              </th>
                              <th style="width: 20%">
                              </th>
                            </tr>
                        </thead>
                        <tbody id="order-display">
                          
                        </tbody>
                        <tfoot>
                            <tr>
                              <th style="width: 1%">
                                  #
                              </th>
                              <th style="width: 20%">
                                  detail
                              </th>
                              <th style="width: 30%">
                                  Nama client
                              </th>
                              <th>
                                  Total
                              </th>
                              <th style="width: 8%" class="text-center">
                                  Status
                              </th>
                              <th style="width: 20%">
                              </th>
                            </tr>
                        </tfoot>
                    </table>
                  </div>
                  <div class="tab-pane fade" id="custom-tabs-one-messages" role="tabpanel" aria-labelledby="custom-tabs-one-messages-tab">
                    <table class="table table-striped projects" id="table-finish">
                        <thead>
                            <tr>
                              <th style="width: 1%">
                                  #
                              </th>
                              <th style="width: 20%">
                                  detail
                              </th>
                              <th style="width: 30%">
                                  Nama client
                              </th>
                              <th>
                                  Total
                              </th>
                              <th style="width: 8%" class="text-center">
                                  Status
                              </th>
                              <th style="width: 20%">
                              </th>
                            </tr>
                        </thead>
                        <tbody id="finish-display">
                            
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>
      <!-- /.modal -->
      <div class="modal fade" id="finish-modal">
        <div class="modal-dialog modal-md">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Selesai</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form>
            <div class="modal-body">
              Beri komentar untuk customer
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" id="1id-transaction" name="1id-transaction">
                  <input type="hidden" id="1id-user" name="1id-user">
                  <textarea class="form-control" id="input-desc" name="desc" placeholder="Isi komentar ..."></textarea>
                </div>
              </div>
            </div>
            <div class="modal-footer justify-content-between">
              <a class="btn bg-default" data-dismiss="modal">Tidak</a>
              <a class="btn bg-cyan" id="btn_finish">Submit</a>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

