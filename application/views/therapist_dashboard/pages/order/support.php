<script>
  $(document).ready(function(){
    show_order();
    show_finish();

    function show_order(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/mitra/0',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;
                  var status_button = '';
                  var status_badge = '';

                  if(obj.data[i].status_transaction == '0'){
                    status_button = '<a class="btn btn-primary" href="#">'+
                                        'Paid '+
                                        '<i class="fas fa-angle-right">'+
                                        '</i>'+
                                    '</a> ';
                    status_badge = '<span class="badge badge-danger">new</span> ';
                  }else if(obj.data[i].status_transaction == '1'){
                    status_button = '<a class="btn btn-primary" href="#">'+
                                        'Finish '+
                                        '<i class="fas fa-angle-double-right">'+
                                        '</i>'+
                                    '</a> ';
                    status_badge = '<span class="badge badge-danger">paid</span> ';
                  }else if(obj.data[i].status_transaction == '2'){
                    status_button = '<a class="btn btn-primary" href="#">'+
                                        'Withdraw '+
                                        '<i class="fas fa-arrow-circle-down">'+
                                        '</i>'+
                                    '</a> ';
                    status_badge = '<span class="badge badge-danger">finish</span> ';
                  }else{
                    status_button = '<a class="btn btn-default" href="#">'+
                                        'Terbayar '+
                                        '<i class="fas fa-check">'+
                                        '</i>'+
                                    '</a> ';
                    status_badge = '<span class="badge badge-danger">withdraw</span> ';
                  }

                  html += '<tr>'+
                            '<td>'+
                                '#'+
                            '</td>'+
                            '<td>'+
                                '<a>'
                                    +obj.data[i].gigs_name+','+obj.data[i].gigs_package_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<td>'+
                                obj.data[i].user_name+
                            '</td>'+
                            '<td>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</td>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-success">new</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                status_button+
                                '<a class="btn btn-info btn-sm" href="<?php echo base_url('Therapist_order_control/approve/?transaction=') ?>' + obj.data[i].id + '&gigs=' + obj.data[i].gigs_id + '&package=' + obj.data[i].detail_gigs_id + '&email=' + obj.data[i].email + '">'+
                                    '<i class="fas fa-pencil-alt">'+
                                    '</i>'+
                                '</a>'+
                            '</td>'+
                        '</tr>';
              }
              $('#order-display').html(html);

              $("#table-order").DataTable({
                "responsive": true,
                "autoWidth": false,
                "columnDefs": [{ 
                    "orderable": false,
                    "targets": 0
                }]
              });
          }
      });
    }
    function show_finish(){
      $.ajax({
          type  : 'GET',
          url   : 'http://103.129.223.136:2099/order/mitra/2',
          headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
          async : true,
          dataType : 'text',
          success : function(data){
              var html = '';
              var i;
              var text = data
              obj = JSON.parse(text);
              console.warn(obj);
              for(i=0; i<obj.data.length; i++){
                  var count = i + 1;

                  html += '<tr>'+
                            '<td>'+
                                '#'+
                            '</td>'+
                            '<td>'+
                                '<a>'
                                    +obj.data[i].gigs_name+','+obj.data[i].gigs_package_name+
                                '</a>'+
                                '<br/>'+
                                '<small>'+
                                    obj.data[i].user_mail+
                                '</small>'+
                            '</td>'+
                            '<td>'+
                                obj.data[i].user_name+
                            '</td>'+
                            '<td>'+
                                'Rp. '+obj.data[i].price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") +
                            '</td>'+
                            '<td class="project-state">'+
                                '<span class="badge badge-success">selesai</span>'+
                            '</td>'+
                            '<td class="project-actions text-right">'+
                                '<button class="btn btn-info" onclick="detailFinish(\'' + obj.data[i].transaction_id + '\',\'' + obj.data[i].user_id + '\')">'+
                                    'Tulis rating <i class="fas fa-pencil-alt">'+
                                    '</i>'+
                                '</button>'+
                            '</td>'+
                        '</tr>';
              }
              $('#finish-display').html(html);
              
              $("#table-finish").DataTable({
                "responsive": true,
                "autoWidth": false,
                "columnDefs": [{ 
                    "orderable": false,
                    "targets": 0
                }]
              });
          }
      });
    }
    $('#btn_finish').on('click',function(){

      var id = $('#1id-transaction').val();
      var id1 = $('#1id-user').val();
      var desc = $('#input-desc').val();
      
      request = $.ajax({
                    url: 'http://103.129.223.136:2099/customer-rating/'+id1,
                    type: 'post',
                    headers: {  'access-token': '<?php echo $this->session->userdata('token'); ?>' },
                    data: {
                        transaction_id: id,
                        rating: '4',
                        comment: desc,
                    }
                });
      request.done(function(response) {
          $('#input-desc').val('');
          $('#finish-modal').modal('hide');
      });
      request.fail(function(response) {
          var success = response.success;
          var message = response.message;
          var data = response.data;
      });
    });
  });

  function detailFinish(id,id1){
    document.getElementById("1id-transaction").value = id;
    document.getElementById("1id-user").value = id1;
    $("#finish-modal").modal('show');
  }
</script>