<div style="padding-top: 100px"></div>
<div class="res-p-4" style=" min-height: calc(100% - 320px);">
    <div class="col-12 container row mb-4">
        <!-- ----- TITLE ----- -->
        <div class="col-12 d-flex align-items-end justify-content-between p-0 mt-2">
            <h2 class="color-primary">Semua Layanan</h2>
        </div>

        <hr class="col-12 row mt-1 mb-3 mr-0 ml-0 pl-0 pr-0">

        <div class="col-12 container row grid-3" id="gigsListLayout">
        </div>
    </div>
</div>

<script>
    var request;
    var nestedrequest;
    var gigsData;
    var value;

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    $(document).ready(function() {
        if (request) {
            request.abort();
        }

        request = $.ajax({
            url: 'http://103.129.223.136:2099/public/gigs/0',
            type: 'get',
            data: {

            }
        });

        request.done(function(response) {
            var success = response.success;
            var message = response.message;
            gigsData = response.data;

            console.log(response);

            if (success) {
                for (i = 0; i < gigsData.length; i++) {
                    value = gigsData[i];

                    request = $.ajax({
                        url: 'http://103.129.223.136:2099/gigs-package/' + value.id,
                        type: 'get',
                        data: {

                        }
                    });

                    request.done(function(response) {
                        var success = response.success;
                        var message = response.message;
                        var data = response.data;

                        console.log(response);

                        if (success) {
                            var lowprice = data[0].price;
                            var highprice = data[data.length - 1].price;
                            var priceGigs = "Rp " + numberWithCommas(lowprice) + " - Rp " + numberWithCommas(highprice);

                            $("#priceGigs-" + data[0].gigs_id).append(priceGigs);
                        } else {
                            console.error(response);
                        }
                    });

                    request.fail(function(response) {
                        console.error(response);
                    });
                    
                    $("#gigsListLayout").append(`
                        <div onclick="window.location='<?php echo base_url('detail_layanan?gigs=') ?>` + value.id + `'" style="cursor: pointer;" tabindex="1" class="card-item d-flex flex-column">
                            <img src="<?php echo base_url('assets/package/master/'); ?>` + value.image + `" class="br-12 w-100" style="height: 220px; object-fit: cover;">
                            <div class="d-flex align-items-start justify-content-between card-item-title" style="flex: 1;">
                                <h3 class="mt-2">` + value.name + `</h3>
                                <a href="<?php echo base_url('detail_terapis') ?>?id=` + value.mitra_id + `"><img src="` + value.mitra_photo + `" class="img-profile"></a>
                            </div>
                            <div class="d-flex align-items-center justify-content-between card-item-price">
                                <div class="d-flex align-items-center">
                                    <i class='bx bxs-star color-primary mr-1' style="font-size: 24px;"></i>
                                    <p class="paragraph-small color-primary" id="ratingGigs-` + value.id + `"></p>
                                </div>
                                <p class="paragraph-small color-primary"><strong id="priceGigs-` + value.id + `"></strong></p>
                            </div>
                        </div>
                    `);
                }
                request = $.ajax({
                    url: 'http://103.129.223.136:2099/gigs-rate-averages',
                    type: 'get',
                    data: {

                    }
                });

                request.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.log(response);

                    if (success) {
                        for (i = 0; i < data.length; i++) {
                            var rate = data[i].rate;
                            rate == null ? rate = '-' : rate = rate.toFixed(1);
                            $("#ratingGigs-" +  data[i].gigs_id).append(rate);
                        }
                    } else {
                        console.error(response);
                    }
                });

                request.fail(function(response) {
                    console.error(response);
                });
            } else {
                console.error(response);
            }
        });

        request.fail(function(response) {
            var success = response.success;
            var message = response.message;
            var data = response.data;

            console.error(response);
        });
    });
</script>