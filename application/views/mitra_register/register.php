<div class="col-12 h-100 row m-0 p-0">
    <!-- ----- IMAGE AREA ----- -->
    <div class="col-lg-7 h-100 bg-primary" style="z-index: 1; position: relative">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 818 533" width="100%" height="70%" preserveAspectRatio="none" style="position: absolute; left: 0; right: 0; bottom: 0; z-index: -1;">
            <defs>
                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                    <stop offset="0" stop-color="#fff" stop-opacity="0.4" />
                    <stop offset="1" stop-color="#fff" stop-opacity="0" />
                </linearGradient>
            </defs>
            <path id="Path_13" data-name="Path 13" d="M7180,3009.084s139.191,38.146,260.925-4.065,114.476-137.711,226.009-164.78,127.06,35.085,209.826,50.006S7998,2969.42,7998,2969.42v397.073H7180Z" transform="translate(-7180 -2833.494)" fill="url(#linear-gradient)" />
        </svg>

        <img style="width: calc(100% - 160px); position: absolute; left: 80px; right: 80px; bottom: 200px;" src="<?php echo base_url('assets/img/login.png') ?>">
    </div>

    <!-- ----- LOGIN AREA ----- -->
    <div class="col-lg-5 d-flex flex-column justify-content-center" style="background-color: #F7F7F7; padding: 60px">
        <a href="<?php echo base_url()?>" class="d-flex">
            <i class='bx bx-chevron-left color-primary' style="font-size: 20px; margin-left: -4px"></i>
            <p class="paragraph-small color-primary">Kembali ke beranda</p>
        </a>
        <h1 class="title-big">Daftar Mitra</h1>
        <p class="paragraph-small mt-1">Untuk tetap terkoneksi dengan kami, silahkan mendaftar  nama, alamat email dan password anda!</p>

        <div class="d-flex w-100 align-items-center mt-2 mb-2" style="opacity:  .2">
            <hr style="flex: 1; border-top: #393939 solid 1px">
            <p class="paragraph-small ml-4 mr-4" style="font-style: italic">Isi form dibawah ini</p>
            <hr style="flex: 1; border-top: #393939 solid 1px">
        </div>

        <form id="RegisterForm">
            <div class="input-group flex-nowrap align-items-center" style="position: relative">
                <input id="Username" type="text" class="form-control pt-4 pb-4 pl-4" placeholder="Username" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-user color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
            </div>

            <div class="input-group flex-nowrap align-items-center mt-3" style="position: relative">
                <input id="Email" type="text" class="form-control pt-4 pb-4 pl-4" placeholder="Email" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-mail-send color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
            </div>

            <div class="input-group flex-nowrap align-items-center mt-3" style="position: relative">
                <input id="Password" type="password" class="form-control pt-4 pb-4 pl-4" placeholder="Password" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-lock color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
            </div>
            <br>
            <div>
                &emsp;&emsp;<input type="checkbox" onclick="myFunction()"> Show Password
            </div>

            <div class="input-group flex-nowrap align-items-center mt-3" style="position: relative">
                <input id="Password1" type="password" class="form-control pt-4 pb-4 pl-4" placeholder="Isi Ulang Password" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-lock color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
            </div>
            <br>
            <div>
                &emsp;&emsp;<input type="checkbox" onclick="myFunction1()"> Show Verification Password
            </div>

            <div class="d-flex align-items-center mt-4">
                <button id="RegisterBtn" class="btn btn-primary d-flex align-items-center" style="padding: 12px 40px; font-weight: bold">
                    <p class="paragraph-small">Daftar</p>
                    <div id="RegisterBtnLoading" class="spinner-border spinner-border-sm ml-2" role="status" style="display: none">
                        <span class="sr-only">Loading...</span>
                    </div>
                </button>
                <a href="<?php echo base_url('mitra_login') ?>" class="btn btn-white ml-4" style="padding: 12px 40px;">Masuk</a>
            </div>

            <div id="RegisterError" class="alert alert-danger alert-dismissible mt-4" style="display: none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gagal!</strong> Pastikan anda sudah mengisi data dengan benar!
            </div>

            <div id="VerificationError" class="alert alert-danger alert-dismissible mt-4" style="display: none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gagal!</strong> Password verifikasi anda tidak cocok!
            </div>
        </form>
    </div>
</div>

<script>
    var request;

    function myFunction() {
        var x = document.getElementById("Password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }
    
    function myFunction1() {
        var x = document.getElementById("Password1");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    }

    $("#RegisterForm").submit(function(event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }

        var UsernameValue = document.getElementById('Username').value;
        var EmailValue = document.getElementById('Email').value;
        var PasswordValue = document.getElementById('Password').value;
        var VerificationValue =  document.getElementById('Password1').value;

        var RegisterBtn = document.getElementById('RegisterBtn');
        var RegisterBtnLoading = document.getElementById('RegisterBtnLoading');

        if(PasswordValue == VerificationValue){
            if (EmailValue != null && PasswordValue != null && UsernameValue != null) {
                RegisterBtn.disabled = true;
                RegisterBtnLoading.style.display = 'inline-block';

                request = $.ajax({
                    url: 'http://103.129.223.136:2099/registration/mitra',
                    type: 'post',
                    data: {
                        name: UsernameValue,
                        email: EmailValue,
                        password: PasswordValue
                    }
                });

                request.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.log(response);

                    RegisterBtn.disabled = false;
                    RegisterBtnLoading.style.display = 'none';

                    if (success) {
                        var url = '<?php echo base_url('mitra_login') ?>';
                        var form = $('<form action="' + url + '" method="post"></form>');
                        $('body').append(form);
                        form.submit();
                    } else {
                        document.getElementById('RegisterError').style.display = 'inline-block';
                    }
                });

                request.fail(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.error(response);

                    RegisterBtn.disabled = false;
                    RegisterBtnLoading.style.display = 'none';

                    document.getElementById('RegisterError').style.display = 'inline-block';
                });
            }
        }else{
            document.getElementById('VerificationError').style.display = 'inline-block';
        }
    });
</script>