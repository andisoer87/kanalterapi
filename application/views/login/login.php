<?php
include(APPPATH.'/controllers/google_api_config.php');
?>

<div class="col-12 h-100 row m-0 p-0">
    <!-- ----- IMAGE AREA ----- -->
    <div class="col-lg-7 d-none d-lg-block h-100 bg-primary" style="z-index: 1; position: relative">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 818 533" width="100%" height="70%" preserveAspectRatio="none" style="position: absolute; left: 0; right: 0; bottom: 0; z-index: -1;">
            <defs>
                <linearGradient id="linear-gradient" x1="0.5" x2="0.5" y2="1" gradientUnits="objectBoundingBox">
                    <stop offset="0" stop-color="#fff" stop-opacity="0.4" />
                    <stop offset="1" stop-color="#fff" stop-opacity="0" />
                </linearGradient>
            </defs>
            <path id="Path_13" data-name="Path 13" d="M7180,3009.084s139.191,38.146,260.925-4.065,114.476-137.711,226.009-164.78,127.06,35.085,209.826,50.006S7998,2969.42,7998,2969.42v397.073H7180Z" transform="translate(-7180 -2833.494)" fill="url(#linear-gradient)" />
        </svg>

        <img style="width: calc(100% - 160px); position: absolute; left: 80px; right: 80px; bottom: 200px;" src="<?php echo base_url('assets/img/login.png') ?>">
    </div>

    <!-- ----- LOGIN AREA ----- -->
    <div class="col-lg-5 d-flex flex-column justify-content-center p-4" style="background-color: #F7F7F7;">
        <a href="<?php echo base_url()?>" class="d-flex">
            <i class='bx bx-chevron-left color-primary' style="font-size: 20px; margin-left: -4px"></i>
            <p class="paragraph-small color-primary">Kembali ke beranda</p>
        </a>    
        <h1 class="title-big mt-4">Selamat Datang</h1>
        <p class="paragraph-small mt-1">Untuk tetap terkoneksi dengan kami, silahkan login dengan alamat email dan password anda!</p>

        <a href="<?php echo $google_client->createAuthUrl(); ?>" class="btn btn-primary d-flex align-items-center mt-4 p-0">
            <div class="bg-white" style="border-radius: 8px 0px 0px 8px; padding: 12px 20px; margin: -2px">
                <img style="width: 24px; height: 24px" src="<?php echo base_url('assets/img/google.png') ?>">
            </div>
            <p class="m-1 ml-4 paragraph-medium">Login dengan google</p>
        </a>

        <div class="d-flex w-100 align-items-center mt-2 mb-2" style="opacity:  .2">
            <hr style="flex: 1; border-top: #393939 solid 1px">
            <p class="paragraph-small ml-4 mr-4" style="font-style: italic">atau</p>
            <hr style="flex: 1; border-top: #393939 solid 1px">
        </div>

        <form id="LoginForm">
            <div class="input-group flex-nowrap align-items-center" style="position: relative">
                <input id="Email" type="text" class="form-control pt-4 pb-4 pl-4" placeholder="Email" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-envelope color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
            </div>

            <div class="input-group flex-nowrap align-items-center mt-3" style="position: relative">
                <input id="Password" type="password" class="form-control pt-4 pb-4 pl-4" placeholder="Password" style="border: none; border-radius: 12px; padding-left: 56px !important">
                <i class='bx bx-lock color-black' style="position: absolute; left: 20px; font-size: 20px; z-index: 10"></i>
                <i id="password_toggle" class='bx bx-hide color-black' style="position: absolute; right: 20px; font-size: 20px; z-index: 10; cursor: pointer"></i>
            </div>

            <div class="d-flex align-items-center justify-content-between mt-3">
                <div class="d-flex align-items-center">
                    <input type="checkbox">
                    <p class="paragraph-small ml-2" style="font-style: italic; opacity: .4">Ingat Saya</p>
                </div>

                <a href="#">
                    <p class="paragraph-small" style="font-style: italic; opacity: .4">Lupa Password?</p>
                </a>
            </div>

            <div class="d-flex align-items-stretch flex-column flex-lg-row flex-md-row flex-sm-row mt-4">
                <button id="LoginBtn" class="btn btn-primary d-flex align-items-center justify-content-center" style="padding: 12px 40px; font-weight: bold">
                    <p class="paragraph-small">Login</p>
                    <div id="LoginBtnLoading" class="spinner-border spinner-border-sm ml-2" role="status" style="display: none">
                        <span class="sr-only">Loading...</span>
                    </div>
                </button>
                <a href="<?php echo base_url('register') ?>" class="btn btn-white ml-0 ml-lg-4 ml-md-4 ml-sm-4 mt-4 mt-lg-0 mt-md-0 mt-sm-0" style="padding: 12px 40px;">Buat akun baru</a>
            </div>

            <div id="LoginError" class="alert alert-danger alert-dismissible mt-4" style="display: none">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Gagal!</strong> User tidak ditemukan! pastikan anda mempunyai akun yang terdaftar!
            </div>
        </form>
    </div>
</div>
<script>
    var show = false;
    $("#password_toggle").click(function(event) {
        if (show) {
            $("#password_toggle").removeClass("bx-show-alt");
            $("#password_toggle").addClass("bx-hide");

            $("#Password").attr('type', 'password');
        } else {
            $("#password_toggle").removeClass("bx-hide");
            $("#password_toggle").addClass("bx-show-alt");

            $("#Password").attr('type', 'text');
        }

        show = !show;
    });
</script>

<script>
    var request;
    $("#LoginForm").submit(function(event) {
        event.preventDefault();

        if (request) {
            request.abort();
        }

        var EmailValue = document.getElementById('Email').value;
        var PasswordValue = document.getElementById('Password').value;

        var LoginBtn = document.getElementById('LoginBtn');
        var LoginBtnLoading = document.getElementById('LoginBtnLoading');

        if (EmailValue != null && PasswordValue != null) {
            LoginBtn.disabled = true;
            LoginBtnLoading.style.display = 'inline-block';

            request = $.ajax({
                url: 'http://103.129.223.136:2099/login/customer',
                type: 'post',
                data: {
                    email: EmailValue,
                    password: PasswordValue
                }
            });

            request.done(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;

                console.log(response);

                LoginBtn.disabled = false;
                LoginBtnLoading.style.display = 'none';

                if (success) {
                    var url = '<?php echo base_url('login/check'); ?>';
                    var form = $('<form action="' + url + '" method="post">' +
                        '<input type="url" name="redirect" value="' + window.location.search.toString().substring(10) + '" />' +
                        '<input type="text" name="email" value="' + EmailValue + '" />' +
                        '<input type="text" name="token" value="' + data.token + '" />' +
                        '</form>');
                    $('body').append(form);
                    form.submit();
                } else {
                    document.getElementById('LoginError').style.display = 'inline-block';
                }
            });

            request.fail(function(response) {
                var success = response.success;
                var message = response.message;
                var data = response.data;

                console.error(response);

                LoginBtn.disabled = false;
                LoginBtnLoading.style.display = 'none';

                document.getElementById('LoginError').style.display = 'inline-block';
            });
        }
    });
</script>