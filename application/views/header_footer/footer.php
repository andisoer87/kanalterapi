<!-- ----- FOOTER SECTION ----- -->
<div class="col-12 bg-primary-dark p-0 pt-4 pb-4 res-p-4">
    <div class="container">
        <div class="col-12 row mt-4 mb-3 mr-0 ml-0 pl-0 pr-0 d-flex align-items-center justify-content-between">
            <div class="d-flex align-items-center">
                <i class='bx bx-image-alt bx-lg color-white'></i>
                <h3 class="ml-2 color-white d-none d-lg-block d-md-block d-sm-block">kanal<span style="font-weight: 400;">terapi</span></h3>
            </div>
            <p class="paragraph-small color-white">Copyright 2020 &copy; kanalterapi</p>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('a[href*=\\#]').on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $(this.hash).offset().top - 100
            }, 400);
        });
    });
</script>

<script>
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (currentScrollPos > 50) {
            document.getElementById("header").style.backgroundColor = "rgba(255, 255, 255, 1)";
        } else {
            document.getElementById("header").style.backgroundColor = "rgba(255, 255, 255, 0)";
        }

        if (prevScrollpos > currentScrollPos) {
            if (window.stickySidebar) {
                stickySidebar.options.topSpacing = 100;
            }
            document.getElementById("header").style.top = "0";
        } else {
            if (window.stickySidebar) {
                stickySidebar.options.topSpacing = 40;
            }
            document.getElementById("header").style.top = "-100px";
        }
        prevScrollpos = currentScrollPos;
    }
</script>

</body>

</html>