<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Kanalterapi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <!-- STYLE -->
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/bootstrap/bootstrap.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/boxicon/boxicons.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/kanalterapi.css'>

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <!-- JAVA SCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc=" crossorigin="anonymous"></script>
    <script src='<?php echo base_url('assets/'); ?>js/bootstrap.js'></script>

    <script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
</head>

<body>