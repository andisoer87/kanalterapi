<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Kanalterapi</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <meta property="og:url"           content="htts://www.kanalterapi.com" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Kanalterapi" />
    <meta property="og:description"   content="Cari Jasa Terapis Terbaik Untuk Kesehatan Mentalmu" />
    <meta property="og:image"         content="http://kanalterapi.com/assets/img/therapy.png" />

    <!-- STYLE -->
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/bootstrap/bootstrap.css'>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/timepicki@2.0.1/css/timepicki.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />

    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/boxicon/boxicons.css'>
    <link rel='stylesheet' type='text/css' media='screen' href='<?php echo base_url('assets/'); ?>css/kanalterapi.css'>

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet">

    <!-- JAVA SCRIPT -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.js" integrity="sha256-r/AaFHrszJtwpe+tHyNi/XCfMxYpbsRg2Uqn0x3s2zc=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src='<?php echo base_url('assets/'); ?>js/bootstrap.js'></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.25.1/moment.min.js" integrity="sha256-epLhbUf8psw8cUHu0hJ5eabk6CPexNJpCbsc0q4ougI=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/timepicki@2.0.1/js/timepicki.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sticky-sidebar@3.3.1/dist/sticky-sidebar.min.js"></script>
</head>

<body>
    <!-- ----- HEADER ----- -->
    <div class="header-container" id="header">
        <div class="container d-flex align-items-center justify-content-between mt-4 mb-4">
            <a href="<?php echo base_url('') ?>">
                <div class="d-flex align-items-center">
                    <i class='bx bx-image-alt bx-lg color-primary'></i>
                    <h3 class="ml-2 color-primary d-none d-lg-block d-md-block">kanal<span style="font-weight: 400;">terapi</span></h3>
                </div>
            </a>

            <div class="d-flex align-items-center">
                <?php if ($this->session->userdata('token') != null) { ?>
                    <a href="<?php echo base_url('') ?>" class="mr-4">
                        <p class="paragraph-medium">Beranda</p>
                    </a>
                    <a href="<?php echo base_url('list_terapis') ?>" class="mr-4">
                        <p class="paragraph-medium">List Terapis</p>
                    </a>
                    <a href="<?php echo base_url('order') ?>" class="mr-4">
                        <p class="paragraph-medium">Order</p>
                    </a>

                    <div class="dropdown">
                        <button class="d-flex bg-transparent align-items-center border-0" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <p class="paragraph-medium d-none d-lg-block d-md-block"><?php echo $this->session->userdata('email'); ?></p>
                            <?php if ($this->session->userdata('image') != null) { ?>
                                <img class="img-profile ml-2 d-flex justify-content-center align-items-center" src="<?php echo $this->session->userdata('image'); ?>">
                            <?php } else { ?>
                                <a href="#" class="img-profile ml-2 d-flex justify-content-center align-items-center" style="background-color: rgba(57, 57, 57, 0.08)">
                                    <h2><?php echo substr($this->session->userdata('email'), 0, 1) ?></h2>
                                </a>
                            <?php } ?>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" data-toggle="modal" data-target="#Profile">Profil</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#Password">Ubah Password</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="<?php echo base_url('login/logout') ?>">Keluar</a>
                        </div>
                    </div>
                <?php } else { ?>
                    <a href="<?php echo base_url('') ?>" class="mr-4">
                        <p class="paragraph-medium">Beranda</p>
                    </a>
                    <a href="<?php echo base_url('list_terapis') ?>" class="mr-4">
                        <p class="paragraph-medium">List Terapis</p>
                    </a>
                    <a href="<?php echo base_url('mitra_register') ?>" class="mr-4 d-none d-lg-block d-md-block">
                        <p class="paragraph-medium">Gabung Terapis</p>
                    </a>
                    <a href="<?php echo base_url('login') ?>" class="mr-4">
                        <p class="paragraph-medium">Masuk</p>
                    </a>
                    <a href="<?php echo base_url('register') ?>" type="button" class="btn btn-primary">Daftar</a>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php if ($this->session->userdata('token') != null) { ?>
        <div class="modal fade" id="Profile" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form id="profileForm">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Profile</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" id="UsernameInput" class="form-control" placeholder="Username">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Affiliate Code</label>
                                <input type="text" id="AffiliateCode" class="form-control" placeholder="AffiliateCode" disabled>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" id="EmailInput" class="form-control" placeholder="Email" disabled>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a type="button" class="btn btn-light color-black" data-dismiss="modal">Batal</a>
                            <button type="submit" class="btn btn-primary color-white">Update</button>
                        </div>
                    </form>

                    <script>
                        
                        var request;

                        $("#profileForm").submit(function(event) {
                            event.preventDefault();

                            if (request) {
                                request.abort();
                            }

                            var NameValue = document.getElementById('UsernameInput').value;

                            console.log(NameValue);

                            if (NameValue) {
                                request = $.ajax({
                                    url: 'http://103.129.223.136:2099/me/customer',
                                    type: 'put',
                                    headers: {
                                        'access-token': '<?php echo $this->session->userdata('token'); ?>'
                                    },
                                    data: {
                                        name: NameValue,
                                    }
                                });

                                request.done(function(response) {
                                    var success = response.success;
                                    var message = response.message;
                                    var data = response.data;

                                    console.log(response);


                                    if (success) {
                                        alert("Berhasil mengubah password");
                                        $('#Profile').modal('toggle');
                                    } else {
                                        console.error(response);
                                        alert(message);
                                    }
                                });

                                request.fail(function(response) {
                                    var success = response.success;
                                    var message = response.message;
                                    var data = response.data;

                                    console.error(response);
                                });
                            }
                        });
                    </script>
                </div>
            </div>
        </div>

        <div class="modal fade" id="Password" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form id="passwordForm">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Change Password</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password Lama</label>
                                <input type="password" id="PasswordOld" class="form-control" placeholder="PasswordOld">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password Baru</label>
                                <input type="password" id="PasswordNew" class="form-control" placeholder="PasswordNew">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a type="button" class="btn btn-light color-black" data-dismiss="modal">Batal</a>
                            <button type="submit" class="btn btn-primary color-white">Update</button>
                        </div>
                    </form>

                    <script>
                        
                        var request;

                        $("#passwordForm").submit(function(event) {
                            event.preventDefault();

                            if (request) {
                                request.abort();
                            }

                            var PasswordOld = document.getElementById('PasswordOld').value;
                            var PasswordNew = document.getElementById('PasswordNew').value;

                            if (PasswordOld && PasswordNew) {
                                request = $.ajax({
                                    url: 'http://103.129.223.136:2099/password/customer',
                                    type: 'post',
                                    headers: {
                                        'access-token': '<?php echo $this->session->userdata('token'); ?>'
                                    },
                                    data: {
                                        old_password: PasswordOld,
                                        new_password: PasswordNew
                                    }
                                });

                                request.done(function(response) {
                                    var success = response.success;
                                    var message = response.message;
                                    var data = response.data;

                                    console.log(response);


                                    if (success) {
                                        alert("Berhasil mengubah password");
                                        $('#Password').modal('toggle');
                                    } else {
                                        alert(message);
                                        console.error(response);
                                    }
                                });

                                request.fail(function(response) {
                                    var success = response.success;
                                    var message = response.message;
                                    var data = response.data;

                                    console.error(response);
                                });
                            }
                        });
                    </script>
                </div>
            </div>
        </div>

        <script>
            var requestUserData;
            var userData;

            $(document).ready(function() {
                if (requestUserData) {
                    requestUserData.abort();
                }

                requestUserData = $.ajax({
                    url: 'http://103.129.223.136:2099/me/customer',
                    type: 'get',
                    headers: {
                        'access-token': '<?php echo $this->session->userdata('token'); ?>'
                    },
                    data: {

                    }
                });

                requestUserData.done(function(response) {
                    var success = response.success;
                    var message = response.message;
                    userData = response.data;

                    console.log(response);

                    if (success) {
                        document.getElementById('UsernameInput').value = userData[0].name;
                        document.getElementById('EmailInput').value = userData[0].email;
                        document.getElementById('AffiliateCode').value = userData[0].affiliate_code;
                    } else {
                        console.error(response);
                    }
                });

                requestUserData.fail(function(response) {
                    var success = response.success;
                    var message = response.message;
                    var data = response.data;

                    console.error(response);
                });
            });
        </script>
    <?php } ?>