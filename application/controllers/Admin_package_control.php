<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_package_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('admin_access') != 'very_allowed'){
			redirect(base_url('admin_login/forbidden'));
		}
	}
 
	public function index()
	{
		$result['page'] = "Paket";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('admin_dashboard/partials/navbar');
		$this->load->view('admin_dashboard/partials/sidebar', $result);
		$this->load->view('admin_dashboard/pages/package/list');
		$this->load->view('admin_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('admin_dashboard/pages/package/support');
	}
 
	public function sub_package()
	{
		$result['page'] = "Paket";

		$sent_data['id_package'] = $this->input->get('var1');
		$sent_data['name_package'] = $this->input->get('var2');

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('admin_dashboard/partials/navbar');
		$this->load->view('admin_dashboard/partials/sidebar', $result);
		$this->load->view('admin_dashboard/pages/sub_package/list');
		$this->load->view('admin_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('admin_dashboard/pages/sub_package/support',$sent_data);
	}
}
