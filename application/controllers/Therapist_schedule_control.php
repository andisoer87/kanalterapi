<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Therapist_schedule_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access') != 'allowed'){
			redirect(base_url('mitra_login/forbidden'));
		}
	}
 
	public function index()
	{
		$navlink['active'] = "nothing";
		$result['page'] = "Jadwal";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('therapist_dashboard/partials/navbar');
		$this->load->view('therapist_dashboard/partials/sidebar', $result);
		$this->load->view('therapist_dashboard/pages/schedule/list');
		$this->load->view('therapist_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('therapist_dashboard/pages/schedule/support');
	}
}
