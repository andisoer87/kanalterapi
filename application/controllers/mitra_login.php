<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mitra_login extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header_footer/w_header');
		$this->load->view('mitra_login/login');
		$this->load->view('header_footer/w_footer');
	}

	public function check()
	{
		$redirect = $this->input->post('redirect');
		$email = $this->input->post('email');
		$token = $this->input->post('token');
		echo $redirect;

		echo $email . " : " . $token;

		if ($email && $token) {
			$sessionData = [
				'access' => 'allowed',
				'email' => $email,
				'token' => $token,
				'login_time' => date("Y/m/d")
			];

			$this->session->set_userdata($sessionData);
			if ($redirect) {
				redirect($redirect);
			} else {
				redirect(base_url('therapist_dashboard_control'));
			}
		} else {
			redirect(base_url('mitra_login'));
		}
	}

	public function forbidden()
	{
		$this->load->view('therapist_dashboard/unallowed/not_allowed');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('mitra_login'));
	}
}
