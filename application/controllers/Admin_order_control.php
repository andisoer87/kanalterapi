<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_order_control extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('admin_access') != 'very_allowed'){
			redirect(base_url('admin_login/forbidden'));
		}
	}

	public function index()
	{
		$result['page'] = "Order";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('admin_dashboard/partials/navbar');
		$this->load->view('admin_dashboard/partials/sidebar', $result);
		$this->load->view('admin_dashboard/pages/order/list');
		$this->load->view('admin_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('admin_dashboard/pages/order/support');
	}

	public function approve()
	{
		$transaction = $this->input->get('transaction');
		$gigs = $this->input->get('gigs');
		$package = $this->input->get('package');
		$email = $this->input->get('email');
		if ($transaction && $gigs && $package && $email) {
			$config = [
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'protocol'  => 'smtp',
				'smtp_host' => 'ssl://smtp.gmail.com',
				'smtp_user' => 'kanalterapi20@gmail.com',    // Ganti dengan email gmail kamu
				'smtp_pass' => 'majujaya20',      // Password gmail kamu
				'smtp_port' => 465,
				'crlf'      => "\r\n",
				'newline'   => "\r\n"
			];

			$this->load->library('email', $config);
			$this->email->from('kanalterapi20@gmail.com', 'kanalterapi.com');
			$this->email->to($email);
			$this->email->subject('Layanan Kanal Terapi : Pilih Jadwal');
			$this->email->message('<h1>Halo! silahkan klik untuk melanjutkan transaksi!</h1><br><a href="http://kanalterapi.com/Schedule?transaction='.$transaction.'&gigs='.$gigs.'&package='.$package.'">Pilih Jadwal</a>');
			if ($this->email->send()) {
				echo "Check Your Email";
			} else {
				echo "gagal mengirim email";
			}
		} else {
			echo 'hah kooosooong';
		}
	}
}
