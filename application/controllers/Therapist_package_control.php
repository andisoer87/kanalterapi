<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Therapist_package_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access') != 'allowed'){
			redirect(base_url('mitra_login/forbidden'));
		}
	}
 
	public function index()
	{
		$navlink['active'] = "nothing";
		$result['page'] = "Paket";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('therapist_dashboard/partials/navbar');
		$this->load->view('therapist_dashboard/partials/sidebar', $result);
		$this->load->view('therapist_dashboard/pages/package/list');
		$this->load->view('therapist_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('therapist_dashboard/pages/package/support');
	}
 
	public function sub_package()
	{
		$navlink['active'] = "nothing";
		$result['page'] = "Paket";

		$sent_data['id_package'] = $this->input->get('var1');
		$sent_data['name_package'] = $this->input->get('var2');

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('therapist_dashboard/partials/navbar');
		$this->load->view('therapist_dashboard/partials/sidebar', $result);
		$this->load->view('therapist_dashboard/pages/sub_package/list');
		$this->load->view('therapist_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('therapist_dashboard/pages/sub_package/support',$sent_data);
	}

	public function upload_file()
	{
		$url = './assets/package/master/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='gif|jpg|png'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$name = $this->input->post('name');
		$desc = $this->input->post('desc');
         
		$this->load->library('upload',$config); //call library upload
		 
        if($this->upload->do_upload("image")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$image = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_name'] = $name;
			$result['data_image'] = $image;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/package/upload', $result);
        }
	}

	public function edit_file()
	{
		$url = './assets/package/master/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='gif|jpg|png'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$id = $this->input->post('edit-id');
		$name = $this->input->post('name');
		$old = $this->input->post('image-old');
		$desc = $this->input->post('desc');
         
		$this->load->library('upload',$config); //call library upload
		 
        if($this->upload->do_upload("image")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$image = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_id'] = $id;
			$result['data_name'] = $name;
			$result['data_image'] = $image;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/package/upload_edit', $result);
        }else{
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$image = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_id'] = $id;
			$result['data_name'] = $name;
			$result['data_image'] = $old;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/package/upload_edit', $result);
		}
	}

	public function upload_class()
	{
		$url = './assets/package/classroom/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='gif|jpg|png'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$name = $this->input->post('name');
		$desc = $this->input->post('desc');
		$title = $this->input->post('title');
		$media = $this->input->post('media');
		$capacity = $this->input->post('capacity');
         
		$this->load->library('upload',$config); //call library upload
		 
        if($this->upload->do_upload("image")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$image = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_name'] = $name;
			$result['data_image'] = $image;
			$result['data_desc'] = $desc;
			$result['data_title'] = $title;
			$result['data_media'] = $media;
			$result['data_capacity'] = $capacity;

			$this->load->view('therapist_dashboard/pages/package/upload_class', $result);
        }
	}

	function proccess_class_upload(){
		$sent_data['data_key'] = $this->input->get('par1');
		$sent_data['data_title'] = $this->input->get('par2');
		$sent_data['data_media'] = $this->input->get('par3');
		$sent_data['data_capacity'] = $this->input->get('par4');

		$this->load->view('therapist_dashboard/pages/package/proccess_upload_class', $sent_data);
	}
}
