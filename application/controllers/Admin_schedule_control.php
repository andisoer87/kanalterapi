<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_schedule_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('admin_access') != 'very_allowed'){
			redirect(base_url('admin_login/forbidden'));
		}
	}
 
	public function index()
	{
		$result['page'] = "Jadwal";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('admin_dashboard/partials/navbar');
		$this->load->view('admin_dashboard/partials/sidebar', $result);
		$this->load->view('admin_dashboard/pages/schedule/list');
		$this->load->view('admin_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('admin_dashboard/pages/schedule/support');
	}
}
