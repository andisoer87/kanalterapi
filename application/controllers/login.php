<?php
defined('BASEPATH') or exit('No direct script access allowed');

class login extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header_footer/w_header');
		$this->load->view('login/login');
		$this->load->view('header_footer/w_footer');
	}

	public function check()
	{
		include(APPPATH . '\controllers\google_api_config.php');

		if (isset($_GET["code"])) {
			$token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);
			if (!isset($token['error'])) {
				$google_client->setAccessToken($token['access_token']);
				$_SESSION['access_token'] = $token['access_token'];
				$google_service = new Google_Service_Oauth2($google_client);
				$data = $google_service->userinfo->get();
				$sessionData = [
					'email' => $data['email'],
					'token' => $token['access_token'],
					'image' => $data['picture'],
					'login_time' => date("Y/m/d")
				];
	
				$this->session->set_userdata($sessionData);

				redirect(base_url(''));
			}
		} else {
			$redirect = $this->input->post('redirect');
			$email = $this->input->post('email');
			$token = $this->input->post('token');
			echo $redirect;
	
			echo $email . " : " . $token;
	
			if ($email && $token) {
				$sessionData = [
					'email' => $email,
					'token' => $token,
					'login_time' => date("Y/m/d")
				];
	
				$this->session->set_userdata($sessionData);
				if ($redirect) {
					redirect($redirect);
				} else {
					redirect(base_url(''));
				}
			} else {
				redirect(base_url('login'));
			}
		}
	}

	public function logout()
	{

		include(APPPATH . '\controllers\google_api_config.php');

		$google_client->revokeToken();

		$this->session->sess_destroy();
		redirect(base_url(''));
	}
}
