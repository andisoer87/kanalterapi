<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Therapist_dashboard_control extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('access') != 'allowed'){
			redirect(base_url('mitra_login/forbidden'));
		}
	}
 
	public function index()
	{
		$navlink['active'] = "Home";
		$result['page'] = "Dashboard";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('therapist_dashboard/partials/navbar');
		$this->load->view('therapist_dashboard/partials/sidebar', $result);
		$this->load->view('therapist_dashboard/dashboard');
		$this->load->view('therapist_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('therapist_dashboard/dashboard_support');
	}
 
	public function my_profile()
	{
		$navlink['active'] = "Profile";
		$result['page'] = "Profile";

		$this->load->view('therapist_dashboard/libraries/header');
		$this->load->view('therapist_dashboard/partials/navbar', $navlink);
		$this->load->view('therapist_dashboard/partials/sidebar', $result);
		$this->load->view('therapist_dashboard/pages/profile/biodata');
		$this->load->view('therapist_dashboard/partials/footer');
		$this->load->view('therapist_dashboard/libraries/script');
		$this->load->view('therapist_dashboard/pages/profile/support');
	}

	public function cv_upload(){
		$url = './assets/mitra/cv/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='*'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$name = $this->input->post('1name-safe');
		$phone = $this->input->post('1telp-safe');
		$porto = $this->input->post('1porto-safe');
		$desc = $this->input->post('1desc-safe');
		
		$this->load->library('upload',$config); //call library upload
		 
        if($this->upload->do_upload("cv")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$cv = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_name'] = $name;
			$result['data_phone'] = $phone;
			$result['data_cv'] = $cv;
			$result['data_porto'] = $porto;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/profile/upload_file', $result);
        }
	}

	public function pt_upload(){
		$url = './assets/mitra/pt/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='*'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$name = $this->input->post('2name-safe');
		$phone = $this->input->post('2telp-safe');
		$cv = $this->input->post('2cv-safe');
		$desc = $this->input->post('2desc-safe');
		
		$this->load->library('upload',$config); //call library upload
		 
        if($this->upload->do_upload("pt")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$porto = $data['upload_data']['file_name']; //set file name ke variable image

			$result['data_name'] = $name;
			$result['data_phone'] = $phone;
			$result['data_cv'] = $cv;
			$result['data_porto'] = $porto;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/profile/upload_file', $result);
        }
	}

	public function profile_upload(){
		$url = './assets/mitra/photo/';
		$config['upload_path'] = $url; //path folder file upload
        $config['allowed_types'] ='*'; //type file yang boleh di upload
        $config['encrypt_name'] = TRUE; //enkripsi file name upload

		$name = $this->input->post('3name-safe');
		$phone = $this->input->post('3telp-safe');
		$cv = $this->input->post('3cv-safe');
		$pt = $this->input->post('3porto-safe');
		$desc = $this->input->post('3desc-safe');
		
		$this->load->library('upload',$config); //call library upload
        if($this->upload->do_upload("gambar")){ //upload file
			$data = array('upload_data' => $this->upload->data()); //ambil file name yang diupload
			
			$photo = "https://kanalterapi.com/assets/mitra/photo/".$data['upload_data']['file_name']; //set file name ke variable image

			$result['data_name'] = $name;
			$result['data_phone'] = $phone;
			$result['data_cv'] = $cv;
			$result['data_pt'] = $pt;
			$result['data_photo'] = $photo;
			$result['data_desc'] = $desc;

			$this->load->view('therapist_dashboard/pages/profile/upload_profile', $result);
        }else{
			echo "upload gagal";
		}
	}
}
