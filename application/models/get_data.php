<?php
// Penduduk.php
class Get_data extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	}
 
	public function graph()
	{
		$data = $this->db->query("SELECT * from data_1");
		return $data->result();
    }
    
    public function summary(){
        $data = $this->db->query("SELECT SUM(jumlah) AS total FROM data_1");
        return $data->result();
    }
 
}